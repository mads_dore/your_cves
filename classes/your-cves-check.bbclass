# your-cves-check.bbclass
#
# This class is used to check recipes against public CVEs,
# including handling of corporate CVE Quality Administation
# within Yocto based development. This including HTML based
# CVE lists and CVE QA overviews to be used during develop-
# ment and together with release delivery.
#
# It class was spun from the original cvs-check.bbclass in 
# meta-poky as a substitute for this class. Hopefully in
# the future the The Yocto Project will adapt this new 
# functionality.
#
# In order to use this class just inherit the class in the
# local.conf file and it will add the cve_check task for
# every recipe. The task can be used per recipe, per image,
# or using the special cases "world" and "universe". The
# your_cves_check task will generate a JSON file with 
# CVE information about unpatched, whitelisted, patched 
# and not relevant cves found and generate a file in the 
# recipe WORKDIR/your_cves directory. If an image is build it 
# will generate a combined JSON file in DEPLOY_DIR_IMAGE for 
# all the packages used. This JSON file can be run through
# the Python scripts in meta-your-cves/tools to generete
# single HTML report file for Yocto CVE QA and CVE overview
# of the project.
#
# Example:
#   bitbake -c your_cves_check openssl
#   bitbake core-image-sato
#   bitbake -k -c your_cves_check universe
#
# Copyright (C) 2020 Mads Doré ApS, Denmark, on code not
# orignally included in cve-check.bbclass.
#
# This source is released under the MIT License.
#
# DISCLAIMER
#
# This class/tool is meant to be used as support and not
# the only method to check against CVEs. Running this tool
# doesn't guarantee your packages are free of CVEs. 

# The product name that the CVE database uses.  Defaults to BPN, but may need to
# be overriden per recipe (for example tiff.bb sets CVE_PRODUCT=libtiff).

CVE_CHECK_LOG = "${T}/your_cves.log"
CVE_CHECK_TMP_FILE = "${TMPDIR}/your_cves_check"

inherit cve-check

CVE_CHECK_RECIPE_FILE ?= "${CVE_CHECK_DIR}/${PN}"

CVE_CHECK_ADDITIONAL_CPE ?= ""

CVE_CHECK_QA_EVALUATED ?= ""
CVE_CHECK_QA_REVIEWED  ?= ""
CVE_CHECK_QA_COMMENT   ?= ""

CVE_CHECK_SKIP_PRODUCTS ?= ""

CVE_CHECK_FILTER_OUT_NOT_VULNERABLE ?= ""
CVE_CHECK_TMP_MANIFEST ?= "${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.cve.tmp"

# overwrite do_cve_check functionality in cve-check bbclass
python do_cve_check () {
    """
    Check recipe for patched and unpatched CVEs
    """

    if os.path.exists(d.getVar("CVE_CHECK_DB_FILE")):
        cve_dict = your_cve_check_cves(d)
        cve_write_data(d, cve_dict)
    else:
        bb.note("No CVE database found, skipping CVE check")

}

def get_list_of_target_recipes(d):
    """
    Get a list of the recipes that is used creating packages that is
    used on target.
    """
    from oe.packagedata import recipename
    from oe.rootfs import image_list_installed_packages

    pkgs = image_list_installed_packages(d)

    recipes=""
    for pkg, value in pkgs.items():
        # Convert pkg to which recipe it originated from
        recipe = recipename(pkg, d)

        if not recipe and value['provs']:
            # Not found. Attempt to find the provider
            recipe = recipename(value['provs'][0], d)

        if recipe and not recipe in recipes:
            recipes += " " + recipe

    # We also need to add the kernel and the bootloader, since they are
    # also on present on target, and they aren't necessarily in the rootfs
    # recipe.
    for pkg in 'bootloader','kernel':
        recipe = d.get("PREFERRED_PROVIDER_virtual/" + pkg)
        if not recipe in recipes:
            recipes += " " + recipe

    return recipes

def fix_cve_tmp_file_json(file):
    """
    Remove the last ',' and add ']' hereby allowing to interpret the file as
    a correctly formatted json file.
    """

    with open(file, "rb+") as f:
        lines = f.readlines()
        last_lines = lines[-5:]
        bb.note(repr(last_lines))
        f.seek(-1, os.SEEK_END)
        f.truncate()
    with open(file, "a") as f:
        f.write("]")

def manifest_write_header(d, file):
    """
    Add your-cves header to the manifest cve file
    """

    with open(file, 'w') as f:
        # Print header to manifest cve file
        f.write('{ "your_cves_1_0": { \n')
        f.write('"configuration": { \n')
        f.write('"sort_out_not_vulnerable": "' + d.getVar("CVE_CHECK_FILTER_OUT_NOT_VULNERABLE") + '"\n')
        f.write('},\n')
        f.write('"recipes": ')

def manifest_write_trailer(file):
    """
    Add your-cves trailer to the manifest cve file
    """

    # Print trailer
    with open(file, "rb+") as f:
        f.seek(-1, os.SEEK_END)
        f.truncate()
    with open(file, "a") as f:
        f.write("]}}")

def manifest_write_cves_for_target_recipes(cve_tmp_file, manifest_file, recipes):
    """
    Write only cves to manifest cve file for recipes that builds packages that is
    placed on target
    """
    import json

    # Fixup tmp file before being able to use json on it
    fix_cve_tmp_file_json(cve_tmp_file)

    # Filter out recipes that is not present on target
    with open(cve_tmp_file, 'r') as r:
        input_dict = json.load(r)
        output_dict = [x for x in input_dict if x['recipe'] in recipes]

    with open(manifest_file, 'a') as w:
        w.write(json.dumps(output_dict, indent=2))

# Note on why we overwrite the original write rootfs manifest function:
# Whenever a dependency for the build images is being built, an entry is written to
# a temporary cve file. This file is wiped in cve_check_cleanup on each build, hence
# the temporary cve file is constructed on each build with all the dependencies.
# Dependencies also includes native tools / cross tools, which is never placed on target.
# The manifest file created for the rootfs should only display recipes cves for cpes placed on target.
# In the rootfs write manifest file in cve-check bbclass, the temporary cve file is simply copied to
# the rootfs manifest cve file, Hence we overwrite the rootfs manifest function
# to filter out stuff instead of doing just a simple copy. We cannot do this
# before constructing the rootfs, since the knowledge of what is placed in the rootfs
# is not known by all recipes but only the rootfs.
python cve_check_write_rootfs_manifest () {
    """
    Create CVE manifest when building an image
    """
    import shutil
    import datetime

    # If the datebase is not updated _TODAY_, fail the CVE check
    with bb.progress.ProgressHandler(d) as ph, open(os.path.join(d.getVar("TMPDIR"), 'cve_check'), 'r') as cve_f:
        updatedTodayStr = 'CVE database update : ' + str(datetime.date.today())
        if not updatedTodayStr in cve_f.read():
            bb.error("CVE Database not updated today. Consider forcing an update by CVE_DB_UPDATE_INTERVAL=1")

    if d.getVar("CVE_CHECK_COPY_FILES") == "1":
        deploy_file = d.getVar("CVE_CHECK_RECIPE_FILE")
        if os.path.exists(deploy_file):
            bb.utils.remove(deploy_file)

    if os.path.exists(d.getVar("CVE_CHECK_TMP_FILE")):
        deploy_dir = d.getVar("DEPLOY_DIR_IMAGE")
        link_name = d.getVar("IMAGE_LINK_NAME")
        manifest_name = d.getVar("CVE_CHECK_MANIFEST")
        tmp_manifest_name = d.getVar("CVE_CHECK_TMP_MANIFEST")
        cve_tmp_file = d.getVar("CVE_CHECK_TMP_FILE")

        # All rootfs recipes will execute this command.
        # We can't have more than 1 rootfs doing fixes on the same file,
        # Since that would cause race-conditions to that file.
        # Hence before doing any fixups to the cve_tmp_file, copy it
        # to a temporary manifest location, and then do the fixups in there.
        shutil.copyfile(cve_tmp_file, tmp_manifest_name)

        # Construct manifest cve file with correct:
        # header
        # only cves for packages included on target
        # trailer
        manifest_write_header(d, manifest_name)
        recipes = get_list_of_target_recipes(d)
        manifest_write_cves_for_target_recipes(tmp_manifest_name, manifest_name, recipes)
        manifest_write_trailer(manifest_name)
        # Delete tmp manifest file after use
        bb.utils.remove(tmp_manifest_name)

        manifest_link = os.path.join(deploy_dir, "%s.cve" % link_name)
        # If we already have another manifest, update symlinks
        if os.path.islink(manifest_link):
            os.remove(manifest_link)
        os.symlink(os.path.basename(manifest_name), manifest_link)
        bb.plain("Image CVE report stored in: %s" % manifest_name)
}

#your_cves: Minor remake of cve_check version int dictionary in stead of list
def get_patches_cves(d):
    """
    Get patches that solve CVEs using the "CVE: " tag.
    """

    import re

    pn = d.getVar("PN")
    cve_match = re.compile("CVE:( CVE\-\d{4}\-\d+)+")

    # Matches last CVE-1234-211432 in the file name, also if written
    # with small letters. Not supporting multiple CVE id's in a single
    # file name.
    cve_file_name_match = re.compile(".*([Cc][Vv][Ee]\-\d{4}\-\d+)")

    patched_cves = {}

    bb.debug(2, "Looking for patches that solves CVEs for %s" % pn)
    for url in src_patches(d):
        patch_file = bb.fetch.decodeurl(url)[2]

        # Check patch file name for CVE ID
        fname_match = cve_file_name_match.search(patch_file)
        if fname_match:
            cve = fname_match.group(1).upper()
            patched_cves[cve] = "Patched with "+patch_file
            bb.debug(2, "Found CVE %s from patch file name %s" % (cve, patch_file))

        with open(patch_file, "r", encoding="utf-8") as f:
            try:
                patch_text = f.read()
            except UnicodeDecodeError:
                bb.debug(1, "Failed to read patch %s using UTF-8 encoding"
                        " trying with iso8859-1" %  patch_file)
                f.close()
                with open(patch_file, "r", encoding="iso8859-1") as f:
                    patch_text = f.read()

        # Search for one or more "CVE: " lines
        text_match = False
        for match in cve_match.finditer(patch_text):
            # Get only the CVEs without the "CVE: " tag
            cves = patch_text[match.start()+5:match.end()]
            for cve in cves.split():
                bb.debug(2, "Patch %s solves %s" % (patch_file, cve))
                patched_cves[cve] = "Patched with "+patch_file
                text_match = True

        if not fname_match and not text_match:
            bb.debug(2, "Patch %s doesn't solve CVEs" % patch_file)

    return patched_cves

# your_cves: Checking for cves of the cpe
def check_cves_for_cpe(conn, vendor, product, version, cpe_patched_cves, cpe_whitelisted_cves, skip_not_vulnerable):

    from distutils.version import LooseVersion

    cves_unpatched                  = []
    cves_unpatched_not_vulnerable   = []
    cves_whitelisted                = []
    cves_patched                    = []   

    returnValue = { 'cpe': vendor+':'+product+':'+version,
                    'unpatched': [], 
                    'unpatched_not_vulnerable': [],
                    'whitelisted': [],
                    'patched': [] }

    # Find all relevant CVE IDs.
    for cverow in conn.execute("SELECT DISTINCT ID FROM PRODUCTS WHERE PRODUCT IS ? AND VENDOR LIKE ?", \
                               (product, vendor)):
        cve = cverow[0]

        if cve in cpe_whitelisted_cves.keys():
            bb.note("%s:%s:%s has been whitelisted for %s" % (vendor, product, version, cve))
            # TODO: this should be in the report as 'whitelisted'
            cves_whitelisted.append({ 'ID': cve, 'comment': cpe_whitelisted_cves[cve]})
            continue

        elif cve in cpe_patched_cves.keys():
            bb.note("%s:%s:%s has been patched for %s" % (vendor, product, version, cve))
            cves_patched.append({ 'ID': cve, 'comment': cpe_patched_cves[cve]})
            continue

        vulnerable = False
        vulnerable_comment = ""
        vulnerable_check_warning = False
        for row in conn.execute("SELECT * FROM PRODUCTS WHERE ID IS ? AND PRODUCT IS ? AND VENDOR LIKE ?", \
                                (cve, product, vendor)):
            (_, _, _, version_start, operator_start, version_end, operator_end) = row

            # bb.note("TESTING('%s', '%s', '%s'): %s" % (str(operator_start), str(version), str(version_start), str(row)))

            if (operator_start == '=' and version == version_start):
                vulnerable = True                
            else:
                if operator_start:
                    try:
                        vulnerable_start  =  (operator_start == '>=' and LooseVersion(version) >= LooseVersion(version_start))
                        vulnerable_start |= (operator_start == '>' and LooseVersion(version) > LooseVersion(version_start))
                    except:
                        bb.warn("%s:%s Failed to compare %s %s %s for %s" %
                                (vendor, product, version, operator_start, version_start, cve))
                        vulnerable_comment += ("%s:%s Failed to compare %s %s %s for %s" %
                                               (vendor, product, version, operator_start, version_start, cve))
                        vulnerable_start = False
                        vulnerable_check_warning = True
                else:
                    vulnerable_start = False

                if operator_end:
                    try:
                        vulnerable_end  = (operator_end == '<=' and LooseVersion(version) <= LooseVersion(version_end))
                        vulnerable_end |= (operator_end == '<' and LooseVersion(version) < LooseVersion(version_end))
                    except:
                        bb.warn("%s:%s Failed to compare %s %s %s for %s" %
                                (vendor, product, version, operator_end, version_end, cve))
                        vulnerable_comment += ("%s:%s Failed to compare %s %s %s for %s" %
                                               (vendor, product, version, operator_start, version_start, cve))
                        vulnerable_end = False
                        vulnerable_check_warning = True
                else:
                    vulnerable_end = False

                if operator_start and operator_end:
                    vulnerable = vulnerable_start and vulnerable_end
                else:
                    vulnerable = vulnerable_start or vulnerable_end
            
            if vulnerable:
                break

        if vulnerable or vulnerable_check_warning:
            if vulnerable_check_warning:
                bb.note("%s:%s:%s vulnerability check failed to %s" % (vendor, product, version, cve))
                cves_unpatched.append({ 'ID': cve, 'comment': vulnerable_comment})
            else:
                bb.note("%s:%s:%s is vulnerable to %s" % (vendor, product, version, cve))
                cves_unpatched.append({ 'ID': cve, 'comment': vulnerable_comment})
        else:
            bb.note("%s:%s:%s is not vulnerable to %s" % (vendor, product, version, cve))
            if not skip_not_vulnerable:
                cves_unpatched_not_vulnerable.append({ 'ID': cve, 'comment': vulnerable_comment})

    returnValue['unpatched'] = cves_unpatched
    returnValue['unpatched_not_vulnerable'] = cves_unpatched_not_vulnerable
    returnValue['whitelisted'] = cves_whitelisted
    returnValue['patched'] = cves_patched

    return returnValue

# your_cves: Check cves of a given recipe against the CVE database.
def your_cve_check_cves(d):
    from oe.cve_check import decode_cve_status
    
    returnValue = { 'recipe': d.getVar("PN"),
                    'version': d.getVar("PV").split("+git")[0],
                    'cve_qa': { 'evaluated_id': "", 'evaluated_date': "", \
                                'reviewed_id': "" , 'reviewed_date': "",   \                     
                                'comments': "" },
                    "skipped": {},
                    'products': [],
                    'additional_cpes': [],
                    'cpes': [],
                    'whitelisted_no_cve': [],
                    'patched_no_cve': [],
                    'stats': { 'patched': 0, 'unpatched': 0, \
                               'whitelisted': 0, 'unpatched_not_vulnerable': 0} 
                }

    # CVE_PRODUCT can contain more than one product (eg. curl/libcurl)
    if d.getVar("CVE_CHECK_SKIP_PRODUCTS"):
        returnValue['skipped'].update({'products':d.getVar("CVE_CHECK_SKIP_PRODUCTS")})
        products = []    
        products_skipped = True
    else:
        products = d.getVar("CVE_PRODUCT").split()
        products_skipped = False
    additional_cpes = d.getVar("CVE_CHECK_ADDITIONAL_CPE").split()


    returnValue['additional_cpes'] = additional_cpes
    returnValue['products'] = products

    if d.getVar("CVE_CHECK_QA_EVALUATED"):
        returnValue['cve_qa']['evaluated_id'], returnValue['cve_qa']['evaluated_date'] = \
            d.getVar("CVE_CHECK_QA_EVALUATED").split(":",1)
    if d.getVar("CVE_CHECK_QA_REVIEWED"):
        returnValue['cve_qa']['reviewed_id'], returnValue['cve_qa']['reviewed_date'] = \
            d.getVar("CVE_CHECK_QA_EVALUATED").split(":",1)
    if d.getVar("CVE_CHECK_QA_COMMENT"):
        returnValue['cve_qa']['comments'] = d.getVar("CVE_CHECK_QA_COMMENT")

    cpe_patched_cves = []
    cpe_whitelisted_cves = []

    old_cve_whitelist =  d.getVar("CVE_CHECK_CVE_WHITELIST")
    if old_cve_whitelist:
        bb.warn("CVE_CHECK_CVE_WHITELIST is deprecated, please use CVE_CHECK_WHITELIST.")
    cves_ignored = ""
    for cve in (d.getVarFlags("CVE_STATUS") or {}):
        decoded_status, _, _ = decode_cve_status(d, cve)
        if decoded_status == "Ignored":
            cves_ignored += cve + " "
    cpe_whitelisted_cves = { i : "" for i in cves_ignored.split() }
    if d.getVarFlags("CVE_CHECK_WHITELIST_WITH_COMMENT"):
        cpe_whitelisted_cves.update(d.getVarFlags("CVE_CHECK_WHITELIST_WITH_COMMENT"))

    cpe_patched_cves = get_patches_cves(d)
    if d.getVarFlags("CVE_CHECK_ADDITIONAL_CPE_PATCH"):
        cpe_patched_cves.update(d.getVarFlags("CVE_CHECK_ADDITIONAL_CPE_PATCH"))

    # If products has been unset and no additional cpe's are set then we're not scanning 
    # for CVEs here (for example, image recipes)
    if not products and not additional_cpes:
        if not d.getVar("CVE_CHECK_SKIP_PRODUCTS"):
          returnValue['skipped'].update({'all_cpes':"Neither products nor additional CPE's in recipe."})   
        return returnValue

    # One common version is set for CVE_PRODUCTS, as previous in Yocto.
    products_version = d.getVar("CVE_VERSION").split("+git")[0]

    # If the recipe has been whitlisted we return empty lists
    if d.getVar("PN") in d.getVar("CVE_CHECK_SKIP_RECIPE").split():
        bb.note("Recipe has been whitelisted, skipping check")
        returnValue['skipped'].update({'recipe':d.getVar("Generally whitelisted.")})   
        return returnValue

    import sqlite3

    db_file = d.expand("file:${CVE_CHECK_DB_FILE}?mode=ro")
    conn = sqlite3.connect(db_file, uri=True)

    # For each of the known product names (e.g. curl has CPEs using curl and libcurl) make a lookup
    # using the common product version.
    for product in products:
        if ":" in product:
            vendor, product = product.split(":", 1)
        else:
            vendor = "%"

        returnValue['cpes'].append(check_cves_for_cpe(conn, vendor, product, \
                                                      products_version, cpe_patched_cves, \
                                                      cpe_whitelisted_cves,
                                                      d.getVar("CVE_CHECK_FILTER_OUT_NOT_VULNERABLE")))
        
    # For each additional cpe make a lookup.
    for cpe in additional_cpes:
        
        #MADO: How to check for precisely 2 ":" in cpe?
        vendor, product, version = cpe.split(":", 2)

        returnValue['cpes'].append(check_cves_for_cpe(conn, vendor, product, \
                                   version, cpe_patched_cves, \
                                   cpe_whitelisted_cves,
                                   d.getVar("CVE_CHECK_FILTER_OUT_NOT_VULNERABLE")))

    conn.close()

    for cpe in returnValue['cpes']:
        for cve in cpe["whitelisted"]:
            cpe_whitelisted_cves.pop(cve['ID'], None)
        for cve in cpe["patched"]:
            cpe_patched_cves.pop(cve['ID'], None)

    returnValue['whitelisted_no_cve'] = cpe_whitelisted_cves
    returnValue['patched_no_cve']     = cpe_patched_cves

    if len(cpe_whitelisted_cves) > 0:
        bb.warn("The following CVE's are marked as whitelisted but are not found in NIST NVD:" + \
                str(cpe_whitelisted_cves))

    if len(cpe_whitelisted_cves) > 0:
        bb.warn("The following CVE's are marked as patched but are not found in NIST NVD:" + \
                str(cpe_patched_cves))

    """
    Get CVE information from the database.
    """

    import sqlite3

    #MADO: Why diggerende from 'd.expand("file:${CVE_CHECK_DB_FILE}?mode=ro")' above?
    conn = sqlite3.connect(d.getVar("CVE_CHECK_DB_FILE"))

    i = 0
    while i < len(returnValue['cpes']):
        returnValue['stats']['whitelisted'] += len(returnValue['cpes'][i]['whitelisted'])
        returnValue['stats']['patched'] += len(returnValue['cpes'][i]['patched'])
        returnValue['stats']['unpatched'] += len(returnValue['cpes'][i]['unpatched'])
        returnValue['stats']['unpatched_not_vulnerable'] += len(returnValue['cpes'][i]['unpatched_not_vulnerable'])
        j = 0
        while j < len(returnValue['cpes'][i]['whitelisted']):
            returnValue['cpes'][i]['whitelisted'][j].update(get_cve_info(conn, \
                                                         returnValue['cpes'][i]['whitelisted'][j]['ID']))
            j += 1        
        j = 0
        while j < len(returnValue['cpes'][i]['patched']):
            returnValue['cpes'][i]['patched'][j].update(get_cve_info(conn, \
                                                     returnValue['cpes'][i]['patched'][j]['ID']))
            j += 1        
        j = 0
        while j < len(returnValue['cpes'][i]['unpatched']):
            returnValue['cpes'][i]['unpatched'][j].update(get_cve_info(conn, \
                                                     returnValue['cpes'][i]['unpatched'][j]['ID']))
            j += 1        
        j = 0
        while j < len(returnValue['cpes'][i]['unpatched_not_vulnerable']):
            returnValue['cpes'][i]['unpatched_not_vulnerable'][j].update(get_cve_info(conn, \
                                                     returnValue['cpes'][i]['unpatched_not_vulnerable'][j]['ID']))
            j += 1
        i += 1

    conn.close()

    # MADO: Add date and unique source identifier, for tracking of Yocto changes and last scan.

    return returnValue

def get_cve_info(conn, cve):

# MADO: Yocto check_cve used 'for row in conn.execute("SELECT * FROM NVD WHERE ID IS ?", (cve,)):'. Why does not make
# that there are multiple hits and if there are they will be overwritten if row[0] is CVE ID?

    for row in conn.execute("SELECT * FROM NVD WHERE ID IS ?", (cve,)):
        returnValue = {}
        returnValue["summary"] = row[1]
        returnValue["scorev2"] = row[2]
        returnValue["scorev3"] = row[3]
        returnValue["modified"] = row[4]
        returnValue["vector"] = row[5]
        returnValue["nvd_link"] = "https://web.nvd.nist.gov/view/vuln/detail?vulnId="+cve

    return returnValue

def cve_write_data(d, cve_dict):
    """
    Write CVE information in WORKDIR; and to CVE_CHECK_DIR, and
    CVE manifest if enabled.
    """

    cve_file = d.getVar("CVE_CHECK_LOG")
    bb.utils.mkdirhier(os.path.dirname(cve_file))

    import json
    bb.note("Writing JSON file %s with CVE information" % cve_file)
    with open(cve_file, 'w') as f:
        json.dump(cve_dict, f, indent=2)

    if d.getVar("CVE_CHECK_COPY_FILES") == "1":
        cve_dir = d.getVar("CVE_CHECK_DIR")
        bb.utils.mkdirhier(cve_dir)
        deploy_file = os.path.join(cve_dir, d.getVar("PN"))
        with open(deploy_file, "w") as f:
            json.dump(cve_dict, f, indent=2)

    if d.getVar("CVE_CHECK_CREATE_MANIFEST") == "1":
        if not os.path.exists(d.getVar("CVE_CHECK_TMP_FILE")):
            with open(d.getVar("CVE_CHECK_TMP_FILE"), "w") as f:
                f.write('[')
        with open(d.getVar("CVE_CHECK_TMP_FILE"), "a") as f:
            write_string = json.dumps(cve_dict, indent=2)
            f.write("%s ," % write_string)                

# EOF
