All recipes that are in the dependency chain are writing to a common file: ${CVE_CHECK_TMP_FILE}, which is set to "${TMPDIR}/your_cves_check".

Each recipe appends to this file.

When a rootfs is build, this file is copied to the cve manifest for the rootfs which is in the deploydir into <name of rootfs>-<MACHINE>-<ARCH>.cve, and after a build is done, this temporary file is removed, i.e. it is created from scratch on each build.

If building something that builds multiple rootfs, then there might be a problem.

Case:
Having 2 rootfs's, A and B.
A has packages: a, b, and c.
B has packages: c, d, e.

The cve manifest is copied from the temporary file.
If building something that builds A and B as part of a dependency chain, then there might be cases where the B rootfs cve manifest might contains a and b, depending on whether the cve checking is done before the pil-rootfs is constructed.
Hence; We got race conditions that might mean that sometimes we get information into our cve manifest regarding packages that aren't even in the rootfs.

This can vary from build to build, since it is dependent on the task length of each recipe, which is dependent on the cve data that needs to be processed.
This can vary from from build machine to build machine, since it is dependent on the number of parallel tasks being run, but even setting the number of threads to 1 will still cause problems.

We cannot fix it by creating dependencies between the rootfs's, because the problem is not the order in which the rootfs's is build, but instead the order in which the dependencies are build.

The propor solution will most likely be to hook into Yocto's package based system and using cve list packages created in each recipe and then passed as dependencies to the rootfs.
