#!/usr/bin/env python3
# json_to_html.py
#
# This script is used to generate stand-alone HTML files,
# for Yocto CVE QA and CVE overviews, with your CVEs 
# JSON files as input (generate in Yocto with 
# your_cves_checks.
#
# Copyright (C) 2020 Mads Doré ApS, Denmark.
#
# This source is released under the MIT License.
#
# DISCLAIMER
#
# This class/tool is meant to be used as support and not
# the only method to check against CVEs. Running this tool
# doesn't guarantee your packages are free of CVEs.

import json
import jsonref
import getopt, sys
from os.path import join, dirname, basename,getmtime
from pprint import pprint, pformat
from datetime import datetime

# CMD line options

full_cmd_arguments  = sys.argv
argument_list       = full_cmd_arguments[1:]

short_options       = 'hcqr'
long_options        = ['help','cve','quick_fix','reduced']

def _print_help():
    print('Usage: json_to_html [OPTION]... filename')
    print('Reads a your_cves JSON file and converts to html.')
    print('')
    print('  -h, --help             This  help text.')
    print('  -c, --cve              CVE\'s only, no Yocto QA.')
    print('  -q, --quick_fix        Quick fix Yocto issue with target files, removes all \'NO EVAL\' recipes.')
    print('  -r, --reduced          Reduced output, filter away CVE\'s marked as \'not vulnerable\'.')

try:    
    options, remained = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    print(str(err))
    print("")
    _print_help()
    sys.exit(2)

option_cve             = False
option_quick_fix       = False
option_reduced         = False

for opt, arg in options:
    if opt in ('-h', '--help'):
        _print_help()
        sys.exit(2)
    elif opt in ('-c', '--cve'):
        option_cve = True
    elif opt in ('-q', '--quick_fix'):
        option_quick_fix = True
    elif opt in ('-r', '--reduce'):
        option_reduced = True


# Get JSON files and generaly need informations
YC_json_file = join(dirname(__file__), remained[0])

input_file_info = basename(YC_json_file) + "  (" + datetime.fromtimestamp(getmtime(YC_json_file)).strftime("%Y/%m/%d, %H:%M:%S")+")"

with open(YC_json_file) as json_file:
    YC_data = json.load(json_file)

if not YC_data["your_cves_1_0"]:
    print("ERROR no your_cves_1_0 dictionary found in JSON file.")
    quit()

use_newline = "\n"

# Output functionality is made at the bottom of the file.

import html

# ====================================================================================================================
# CVE only HTML page

# --------------------------------------------------------------------------------------------------------------------
def your_cves_html_make_cve_file(input_file_info, reduced):

    include_not_vulnerable = False if reduced else (YC_data["your_cves_1_0"]["configuration"]["sort_out_not_vulnerable"] == "")

    print(your_cves_html_begin(input_file_info, include_not_vulnerable, False)) 
    print(your_cves_cve_view(include_not_vulnerable, True))

    print(your_cves_html_end())
    quit()

# ====================================================================================================================
# Yocto QA HTML page

# --------------------------------------------------------------------------------------------------------------------
def your_cves_html_make_yocto_qa_file(input_file_info, reduced):

    include_not_vulnerable = False if reduced else (YC_data["your_cves_1_0"]["configuration"]["sort_out_not_vulnerable"] == "")

    print(your_cves_html_begin(input_file_info, include_not_vulnerable, True)) 
    print(your_cves_yocto_view(include_not_vulnerable))
    print(your_cves_cve_view(include_not_vulnerable, False))

    print(your_cves_html_end())
    quit()

# ====================================================================================================================
# Generic HTML page areas

# --------------------------------------------------------------------------------------------------------------------
def your_cves_html_begin(input_file_info, include_not_vulnerable, include_yocto):

    html_string  = '<!DOCTYPE html>' + use_newline
    html_string += '<html>' + use_newline
    html_string += '<head>' + use_newline
    html_string += '  <link id="favicon" rel="shortcut icon" type="image/png" href="data:image/png;base64,'
    f = open("../web_page/includes_for_single_page_html/favicon.txt", "r")
    html_string += f.read();
    html_string += '    ">' + use_newline
    html_string += '  <style>' + use_newline
    f = open("../web_page/your_cves.css", "r")
    html_string += f.read();
    html_string += '  </style>' + use_newline

    if not include_not_vulnerable:
        html_string += '  <style>' + use_newline
        html_string += '    div.total_unpatched, div.total_whitelisted, div.total_not_vulnerable, div.total_patched,' + use_newline
        html_string += '    div.total_unpatched_sort, div.total_whitelisted_sort, div.total_not_vulnerable_sort, div.total_patched_sort {' + use_newline
        html_string += '      width: 33.333333%; width: calc(100%/3); }' + use_newline
        html_string += '  </style>' + use_newline

    html_string += '</head>' + use_newline
    html_string += '<body>' + use_newline
    html_string += '  <div class="header">' + use_newline
    html_string += '    <div class="logo_holder"><img src="data:image/png;base64,'
    f = open("../web_page/includes_for_single_page_html/logo_100px.txt", "r")
    html_string += f.read();
    html_string += '    "></div>' + use_newline
    html_string += '    <div class="action_container">' + use_newline
    html_string += '      <div class="view_selector" style="display:block">' + use_newline
    if include_yocto:
        html_string += '        <p><button onclick="$(\'.yocto_view\').show();$(\'.cve_view\').hide();">View CVE QA</button>&nbsp;&nbsp;' + use_newline
        html_string += '           <button onclick="$(\'.yocto_view\').hide();$(\'.cve_view\').show();">View CVE\'s</button></p>' + use_newline
    html_string += '      </div>' + use_newline
    html_string += '    </div>' + use_newline
    html_string += '    <div class="general_info">From: ' + html.escape(input_file_info) + use_newline 
    html_string += '                               </br></br>' + html.escape(YC_data["your_cves_1_0"]["configuration"]["sort_out_not_vulnerable"]) + '</div>' + use_newline      
    html_string += '  </div>' + use_newline
    html_string += '' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def your_cves_html_end():

    html_string  = '</body>' + use_newline
    html_string += '</html>' + use_newline
    html_string += '<script>' + use_newline
    f = open("../web_page/includes_for_single_page_html/jquery-2.2.4.min.js", "r")
    html_string += f.read();
    html_string += '</script>' + use_newline
    html_string += '<script>' + use_newline
    f = open("../web_page/your_cves_sort.js", "r")
    html_string += f.read();
    html_string += '</script>' + use_newline

    return html_string  

# ====================================================================================================================
# CVE view

def your_cves_cve_view(include_not_vulnerable, display):
    
    your_cves_make_cve_dict()

    html_string  =('<div class="cve_view" %s>' + use_newline) % ("" if display else 'style="display:none"')
    html_string += your_cves_cve_make_header()
    html_string += '  <ul id="cveGroupList">' + use_newline
    html_string += your_cves_cve_add_group("unpatched")
    html_string += your_cves_cve_add_group("whitelisted")
    html_string += your_cves_cve_add_group("patched")
    if include_not_vulnerable:
        html_string += your_cves_cve_add_group("unpatched_not_vulnerable")
    html_string += '  </ul>' + use_newline
    html_string += '</div>' + use_newline

    return html_string

YC_cve_dict = {
  'patched': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }},
  'unpatched': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }},
  'whitelisted': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }},
  'unpatched_not_vulnerable': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }}
}

# --------------------------------------------------------------------------------------------------------------------
def your_cves_add_cves_dict_item(group, cpe):
    
    for cve in cpe[group]:

        check_value = cve["ID"] + ":" + cpe["cpe"]

        if not check_value in YC_cve_dict[group]["cves"]:
            YC_cve_dict[group]["cves"].update( { check_value : cve } )        
            YC_cve_dict[group]["counts"][cvss_3_severity(cve["scorev3"])] += 1
        else:
            YC_cve_dict[group]["cves"][check_value]["comment"] += cve["comment"]

# --------------------------------------------------------------------------------------------------------------------
def your_cves_make_cve_dict():

    for recipe in YC_data["your_cves_1_0"]["recipes"]:
        if not((recipe["cve_qa"]["evaluated_id"] == "") and  (recipe["cve_qa"]["evaluated_date"] == "") and 
            option_quick_fix):

            for cpe in recipe["cpes"]:
                your_cves_add_cves_dict_item("patched", cpe)
                your_cves_add_cves_dict_item("whitelisted", cpe)
                your_cves_add_cves_dict_item("unpatched", cpe)
                your_cves_add_cves_dict_item("unpatched_not_vulnerable", cpe)

# --------------------------------------------------------------------------------------------------------------------
def your_cves_cve_make_header():

    html_string  = '    <div class="cve_group_header">' + use_newline
    html_string += '      <div class="cve_group_status_header">CVE Status</div>' + use_newline
    html_string += '      <div class="cve_group_status_actions_header"></div>' + use_newline
    html_string += '      <div class="cve_group_v3_score_header">CVSS V3 Scores</div>' + use_newline
    html_string += '      <div class="cve_group_total_header">Total</div>' + use_newline
    html_string += '    </div>' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def your_cves_cve_add_group(group):

    sort_string  = '<div class="cve_sortline" style="display:block">Sort by \
                       <button data-sort="des" \
                           onclick="your_cves_cve_sort_pressed(this, \'div.cve_id\', \'cve_id\');">ID</button> \
                       <button data-sort="des" \
                           onclick="your_cves_cve_sort_pressed(this, \'div.cve_vendor_product\', \'text\');">Product</button> \
                       <button data-sort="des" \
                           onclick="your_cves_cve_sort_pressed(this, \'div.cve_vector\', \'text\');">Vector</button> \
                       <button data-sort="des" \
                           onclick="your_cves_cve_sort_pressed(this, \'div.cve_V2_score\', \'text\');">V2 Score</button> \
                       <button data-sort="des" \
                           onclick="your_cves_cve_sort_pressed(this, \'div.cve_V3_score\',  \'text\');">V3 Score</button> \
                       <button data-sort="des" \
                           onclick="your_cves_cve_sort_pressed(this, \'div.cve_modified\',  \'text\');">Modified</button> \
                    </div>'

    cve_status = 'Not vulnerable' if (group == 'unpatched_not_vulnerable') else group.capitalize()

    total_cves   = 0
    class_to_add = {}
    count_to_add = {}    
    
    for level, count in YC_cve_dict[group]["counts"].items():
        class_to_add.update( { level : (("cvss_V3_severity_"+level) if (count > 0) else "") } )
        count_to_add.update( { level : str(count) } )
        total_cves += count

    total_class_to_add = "status_"+("not_vulnerable" if (group == "unpatched_not_vulnerable") else group) if (total_cves > 0) else ""

    html_string  = '<li>' + use_newline
    html_string += '  <div class="cve_group_headline">' + use_newline
    html_string +=('    <div class="cve_group_status">%s</div>' + use_newline) % cve_status
    html_string += '    <div class="cve_group_status_actions">' + use_newline
    if (total_cves > 0):
        html_string += '      <button onclick="showCVEs(this);">show</button>' + use_newline
    html_string += '    </div>' + use_newline
    html_string += '    <div class="cve_group_v3_score">' + use_newline
    html_string +=('      <div class="cve_group_v3_critical %s" title="Critical severity according to CVSS v3.x">%s</div>' + use_newline) % (class_to_add["critical"], count_to_add["critical"])
    html_string +=('      <div class="cve_group_v3_high %s" title="High severity according to CVSS v3.x">%s</div>' + use_newline) % (class_to_add["high"], count_to_add["high"])
    html_string +=('      <div class="cve_group_v3_medium %s" title="Medium severity according to CVSS v3.x">%s</div>' + use_newline) % (class_to_add["medium"], count_to_add["medium"])
    html_string +=('      <div class="cve_group_v3_low %s" title="Low severity according to CVSS v3.x">%s</div>' + use_newline) % (class_to_add["low"], count_to_add["low"])
    html_string +=('      <div class="cve_group_v3_none %s" title="No severity according to CVSS v3.x">%s</div>' + use_newline) % (class_to_add["none"], count_to_add["none"])
    html_string += '    </div>' + use_newline
    html_string +=('    <div class="cve_group_total %s">%s</div>' + use_newline) % (total_class_to_add, str(total_cves))
    html_string += '  </div>' + use_newline

    if (total_cves > 0):
        html_string += '  <div class="cve_cves" style="display:none">' + use_newline
        if (total_cves > 5):
            html_string += sort_string    
        html_string += '    <ul>' + use_newline
        
        for check_value, cve in YC_cve_dict[group]["cves"].items():
            
            cve_id, vendor, product, version = check_value.split(":", 3)
            vendor_product = ""
            if not (vendor == "%"):
                vendor_product += vendor + ":"
            vendor_product += product

            html_string += your_cves_add_cve_item(cve, cve_status, vendor_product, version)

        html_string += '    </ul>' + use_newline
        html_string += '  </div>' + use_newline
        html_string += '</li>' + use_newline

    return html_string
    
# ====================================================================================================================
# Yocto view

def your_cves_yocto_view(include_not_vulnerable):
    
    html_string  = '<div class="yocto_view" style="display:block">' + use_newline
    html_string += your_cves_recipe_make_sortline(include_not_vulnerable)
    html_string += '  <ul id="recipeList">' + use_newline
    for recipe in YC_data["your_cves_1_0"]["recipes"]:
        html_string += your_cves_recipe_add_recipe(recipe, include_not_vulnerable)
    html_string += '  </ul>' + use_newline
    html_string += '</div>' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def your_cves_recipe_make_sortline(include_not_vulnerable):

    html_string  = '  <div class="recipe_sortline">' + use_newline
    html_string += '    <div class="recipe_sort" data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.recipe\', \'text\');">Yocto recipe name</div>' + use_newline
    html_string += '    <div class="version_sort" data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.version\', \'text\');">Version</div>' + use_newline
    html_string += '    <div class="total_cves_sort">' + use_newline
    html_string += '        <div class="total_unpatched_sort" title="Number of unpatched CVEs found." data-sort="des" ' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_unpatched\', \'number\');">U</div>' + use_newline
    html_string += '        <div class="total_whitelisted_sort" title="Number of whitelisted CVEs found." data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_whitelisted\', \'number\');">W</div>' + use_newline
    html_string += '        <div class="total_patched_sort" title="Number of patched CVEs found." data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_patched\', \'number\');">P</div>' + use_newline
    if include_not_vulnerable:
        html_string += '        <div class="total_not_vulnerable_sort"' + use_newline
        html_string += '             title="Number of CVEs found for other versions of the relevant CPEs." data-sort="des"' + use_newline
        html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_not_vulnerable\', \'number\');">N</div>' + use_newline  
    html_string += '    </div>' + use_newline
    html_string += '    <div class="yocto_cve_qa_sort data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.yocto_cve_qa\', \'text\');"">Yocto CVE QA</div>' + use_newline
    html_string += '  </div>' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def your_cves_recipe_make_sortline(include_not_vulnerable):

    html_string  = '  <div class="recipe_sortline">' + use_newline
    html_string += '    <div class="recipe_sort" data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.recipe\', \'text\');">Yocto recipe name</div>' + use_newline
    html_string += '    <div class="version_sort" data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.version\', \'text\');">Version</div>' + use_newline
    html_string += '    <div class="total_cves_sort">' + use_newline
    html_string += '        <div class="total_unpatched_sort" title="Number of unpatched CVEs found." data-sort="des" ' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_unpatched\', \'number\');">U</div>' + use_newline
    html_string += '        <div class="total_whitelisted_sort" title="Number of whitelisted CVEs found." data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_whitelisted\', \'number\');">W</div>' + use_newline
    html_string += '        <div class="total_patched_sort" title="Number of patched CVEs found." data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_patched\', \'number\');">P</div>' + use_newline
    if include_not_vulnerable:
        html_string += '        <div class="total_not_vulnerable_sort"' + use_newline
        html_string += '             title="Number of CVEs found for other versions of the relevant CPEs." data-sort="des"' + use_newline
        html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.total_not_vulnerable\', \'number\');">N</div>' + use_newline  
    html_string += '    </div>' + use_newline
    html_string += '    <div class="yocto_cve_qa_sort data-sort="des"' + use_newline
    html_string += '         onclick="your_cves_sort_recipe_pressed(this, \'div.yocto_cve_qa\', \'text\');"">Yocto CVE QA</div>' + use_newline
    html_string += '  </div>' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def your_cves_recipe_add_recipe(recipe, include_not_vulnerable):

    # Make Yocto CVE Quality Assurance information. 
    yocto_cve_qa                = "NOT IMPLEMENTED";
    yocto_cve_qa_eval_style     = "";
    yocto_cve_qa_info           = "";
    yocto_cve_qa_tracking_info  = '<p class="small_heading">Yocto CVE QA tracking:</p>';

    if (recipe["cve_qa"]["evaluated_id"] == "") and  (recipe["cve_qa"]["evaluated_date"] == ""):
        yocto_cve_qa_tracking_info += "No evaluation made.</br>"
        yocto_cve_qa = "NO EVAL"
        yocto_cve_qa_eval_style = "yocto_qa_no_valid_eval"
        if option_quick_fix:
            return ""    
    elif (recipe["cve_qa"]["evaluated_id"] == "") or (recipe["cve_qa"]["evaluated_date"] == ""):
        yocto_cve_qa_tracking_info += "Wrong evaluation note.</br>"
        yocto_cve_qa = "EVAL ERROR"
        yocto_cve_qa_eval_style = "yocto_qa_no_valid_eval"
    else:
      yocto_cve_qa_info += "Evaluation done by "+recipe["cve_qa"]["evaluated_id"]+" on "+recipe["cve_qa"]["evaluated_date"]+".</br>";
      yocto_cve_qa = "EVAL DONE"

      if (recipe["cve_qa"]["reviewed_id"] == "") and (recipe["cve_qa"]["reviewed_date"] == ""):
        yocto_cve_qa_tracking_info += "No review made.</br>"
        yocto_cve_qa = "NO REVIEW"
        yocto_cve_qa_eval_style = "yocto_qa_no_valid_review"
      elif (recipe["cve_qa"]["reviewed_id"] == "") or (recipe["cve_qa"]["reviewed_date"] == ""):
          yocto_cve_qa_tracking_info += "Wrong review note.</br>"
          yocto_cve_qa = "REVIEW ERROR"
          yocto_cve_qa_eval_style = "yocto_qa_no_valid_review"
      else:
          yocto_cve_qa_tracking_info += "Review done by "+recipe["cve_qa"]["reviewed_id"]+" on "+recipe["cve_qa"]["reviewed_date"]+".</br>"
          yocto_cve_qa = "OK"
          yocto_cve_qa_eval_style = "yocto_qa_ok"
  
    if (len(recipe["patched_no_cve"]) > 0) or (len(recipe["whitelisted_no_cve"]) > 0):
    
        # Overwrite on purpose
        yocto_cve_qa            = "CVE errors"
        yocto_cve_qa_eval_style = "yocto_qa_cve_errors"

        if (len(recipe["patched_no_cve"]) > 0):
            yocto_cve_qa_info += \
                '<p class="small_heading">CVEs marked patched with no matching ID for CVE or CPE in NITS NVD:</p>'

            for key, comment in recipe["patched_no_cve"].items():
                yocto_cve_qa_info += key+": "+html.escape(comment)+"</br>"

        if (len(recipe["whitelisted_no_cve"]) > 0):
            yocto_cve_qa_info += \
                '<p class="small_heading">CVEs whitelisted with no matching ID for CVE or CPE in NITS NVD:</p>'

            for key, comment in recipe["whitelisted_no_cve"].items():
                yocto_cve_qa_info += key+": "+html.escape(comment)+"</br>"


    # Make Yocto CPE information.
    cpe_info = ""
    separator = ' , '

    if (len(recipe["products"]) > 0):
        cpe_info += '<p class="small_heading">Recipe products used as CPEs:</p>'
        cpe_info += '<span class="info_lead">Common version:</span> '+html.escape(recipe["version"])+'<br/>'
        cpe_info += '<span class="info_lead">Products:</span> '+html.escape(separator.join(recipe["products"]))
  
    if (len(recipe["additional_cpes"]) > 0):
        cpe_info += '<p class="small_heading">Additional CPEs:</p>'
        cpe_info += '<span class="info_lead">Products:</span> '+html.escape(separator.join(recipe["additional_cpes"]))
  
    comments = ""

    if "products" in recipe["skipped"]: 
        comments += "Skipped: "+html.escape(recipe["skipped"]["products"])

    if "recipe" in recipe["skipped"]: 
        if (comments != ""):
            comments += "</br>"
        comments += "Skipped: "+html.escape(recipe["skipped"]["recipe"])

    if "all_cpes" in recipe["skipped"]: 
        if (comments != ""):
            comments += "</br>"
        comments += "Skipped: "+html.escape(recipe["skipped"]["all_cpes"])

    recipe_details_empty = ""
    if (len(cpe_info) == 0) and (len(yocto_cve_qa_info) == 0):
        recipe_details_empty = 'data-empty="empty"'

    not_vulnerable_class = ""

    total_cves = recipe["stats"]["unpatched"] + recipe["stats"]["whitelisted"] + recipe["stats"]["patched"]
    if (include_not_vulnerable):
        total_cves += recipe["stats"]["unpatched_not_vulnerable"]
        if recipe["stats"]["unpatched_not_vulnerable"]:
            not_vulnerable_class = "status_not_vulnerable"

    unpatched_class = ""
    patched_class = ""
    whitelisted_class = ""

    if recipe["stats"]["whitelisted"]:
        whitelisted_class = "status_whitelisted"
    if recipe["stats"]["patched"]:
        patched_class = "status_patched"
    if recipe["stats"]["unpatched"]:
        unpatched_class = "status_unpatched"

    html_string  = '<li class="recipe_li">' + use_newline
    html_string += '  <div class="recipe_headline">' + use_newline
    html_string += '    <div class="recipe_holder">' + use_newline
    html_string +=('      <div class="recipe">%s</div>' + use_newline) % recipe["recipe"]
    if (total_cves > 0):
        html_string += '      <div class="recipe_actions">' + use_newline
        html_string += '        <button onclick="showAllDetailsRecipe(this);">show</button>' + use_newline
        html_string += '      </div>' + use_newline
    html_string += '    </div>' + use_newline
    html_string +=('    <div class="version">%s</div>' + use_newline) % recipe["version"]
    html_string += '    <div class="total_cves">' + use_newline
    html_string +=('      <div class="total_unpatched %s">%s</div>' + use_newline) % (unpatched_class, recipe['stats']['unpatched'])
    html_string +=('      <div class="total_whitelisted %s">%s</div>' + use_newline) % (whitelisted_class, recipe["stats"]["whitelisted"])
    html_string +=('      <div class="total_patched %s">%s</div>' + use_newline) % (patched_class, recipe["stats"]["patched"])
    if include_not_vulnerable:
        html_string +=('      <div class="total_not_vulnerable %s">%s</div>' + use_newline) % (not_vulnerable_class, recipe["stats"]["unpatched_not_vulnerable"])
    html_string += '    </div>' + use_newline
    html_string +=('    <div class="yocto_cve_qa %s">%s</div>' + use_newline) % (yocto_cve_qa_eval_style, yocto_cve_qa)
    html_string += '  </div>' + use_newline
    html_string += '  <div class="recipe_info" style="display:none">' + use_newline
    html_string +=('    <div class="comments">%s</div>' + use_newline) % comments
    html_string +=('    <div class="yocto_qa_comments">%s</div>' + use_newline) % yocto_cve_qa_tracking_info
    html_string += '  </div>' + use_newline
    html_string +=('  <div class="recipe_details" %s style="display:none">' + use_newline) % recipe_details_empty
    html_string +=('    <div class="recipe_cpes">%s</div>' + use_newline) % cpe_info
    html_string +=('    <div class="recipe_cve_qa">%s</div>' + use_newline) % yocto_cve_qa_info
    html_string += '  </div>' + use_newline
    html_string += your_cves_recipe_make_cve_list(recipe, include_not_vulnerable)
    html_string += '</li>' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def your_cves_recipe_make_cve_list(recipe, include_not_vulnerable):

    sort_string  = '<div class="recipe_cves_sort">Sort by \
                        <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_status\', \'text\');">Status</button> \
                       <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_id\', \'cve_id\');">ID</button> \
                       <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_vendor_product\', \'text\');">Product</button> \
                       <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_vector\', \'text\');">Vector</button> \
                       <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_V2_score\', \'text\');">V2 Score</button> \
                       <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_V3_score\',  \'text\');">V3 Score</button> \
                       <button data-sort="des" \
                           onclick="your_cves_recipe_cves_sort_pressed(this, \'div.cve_modified\',  \'text\');">Modified</button> \
                    </div>'
  
    total_li_items = recipe["stats"]["unpatched"] + recipe["stats"]["whitelisted"] + \
                     recipe["stats"]["unpatched_not_vulnerable"] + recipe["stats"]["patched"]

    html_string = ""

    if (total_li_items > 0):
        html_string += '<div class="recipe_cves" style="display:none">'
        if (total_li_items > 5):
            html_string += sort_string
        html_string += "<ul>"
   
        for cpe in recipe["cpes"]:

            vendor, product, version = cpe["cpe"].split(":", 2)
            vendor_product = ""
            if not (vendor == "%"):
                vendor_product += vendor+":"
            vendor_product += product

            for cve in cpe["patched"]:
                html_string += your_cves_add_cve_item(cve, "Patched", vendor_product, version)    

            for cve in cpe["whitelisted"]:
                html_string += your_cves_add_cve_item(cve, "Whitelisted", vendor_product, version)    

            for cve in cpe["unpatched"]:
                html_string += your_cves_add_cve_item(cve, "Unpatched", vendor_product, version)    

            if (include_not_vulnerable):
                for cve in cpe["unpatched_not_vulnerable"]:
                    html_string += your_cves_add_cve_item(cve, "Not vulnerable", vendor_product, version)    

        html_string += "</ul></div>"
    else:
        html_string += '<div class="recipe_cves" data-empty="empty" style="display:none"></div>'

    return html_string

# ====================================================================================================================
# Generic functions

# --------------------------------------------------------------------------------------------------------------------
def your_cves_add_cve_item(cve, current_cve_status, current_cpe_vendor_product, current_cpe_version):

    cvss_2_severity_class = "cvss_V2_severity_" + cvss_2_severity(cve["scorev2"])
    cvss_3_severity_class = "cvss_V3_severity_" + cvss_3_severity(cve["scorev3"])

    html_string  = '<li>' + use_newline
    html_string += '  <div class="cve_item">' + use_newline
    html_string +=('    <div class="cve_status">%s</div>' + use_newline) % current_cve_status
    html_string +=('    <div class="cve_cpe_vendor_product">%s</div>' + use_newline) % current_cpe_vendor_product
    html_string +=('    <div class="cve_vector">%s</div>' + use_newline) % cve["vector"]
    html_string +=('    <div class="cve_cpe_version">%s</div>' + use_newline) % current_cpe_version
    html_string +=('    <div class="cve_id">%s</div>' + use_newline) % cve["ID"]
    html_string += '    <div class="cve_score">' + use_newline
    html_string +=('       <div class="cve_V2_score %s">V2: %s</div>' + use_newline) % (cvss_2_severity_class, cve["scorev2"])
    html_string +=('       <div class="cve_V3_score %s">V3: %s</div>' + use_newline) % (cvss_3_severity_class, cve["scorev3"])
    html_string += '    </div>' + use_newline
    html_string +=('    <div class="cve_modified">%s</div>' + use_newline) % cve["modified"]
    html_string +=('    <div class="cve_info">%s</div>' + use_newline) % html.escape(cve["summary"])
    if cve["comment"]:
        html_string +=('    <div class="cve_link"><a href="%s" target="_blank">%s</a></div>' + use_newline) % (cve["nvd_link"], cve["nvd_link"])
        html_string +=('    <div class="cve_comments">%s</div>' + use_newline) % html.escape(cve["comment"])
    else:
        html_string +=('    <div class="cve_link last_cve_line"><a href="%s" target="_blank">%s</a></div>' + use_newline) % (cve["nvd_link"], cve["nvd_link"])
        html_string += '    <div class="cve_comments" style="display:none"></div>' + use_newline
    html_string +='  </div>' + use_newline
    html_string +='</li>' + use_newline

    return html_string

# --------------------------------------------------------------------------------------------------------------------
def cvss_2_severity(score):
  
    if (int(float(score)) < 4):
        return "low"
    elif (int(float(score)) < 7):
        return "medium"
 
    return "high"

# --------------------------------------------------------------------------------------------------------------------
def cvss_3_severity(score):

    if (int(float(score)) == 0):
        return "none"
    elif (int(float(score)) < 4):
        return "low"
    elif (int(float(score)) < 7):
        return "medium"
    elif (int(float(score)) < 9):
        return "high"

    return "critical"

# ====================================================================================================================
# Python script execution

if option_reduced:
    YC_data["your_cves_1_0"]["configuration"]["sort_out_not_vulnerable"] = ""

if option_cve:
    your_cves_html_make_cve_file(input_file_info, option_reduced)
else:
    your_cves_html_make_yocto_qa_file(input_file_info, option_reduced)

quit()

# EOF
