# debug_tools.py
#
# This py file add various tools to print debug outputs.
#
# Copyright (C) 2024 Mads Dore ApS, Denmark.

# ---------------------------------------------------------------------------------------------------------------------
# Checks and prints debug output, with indentation, if requested.
def debug_print(debug_level, debug_message, print_end = '\n'):
    if debug_level is None:
        print(debug_message, flush=True, end = print_end)
    else:
        print((' '*2*debug_level)+debug_message, flush=True, end = print_end)
    # LLOF
      
# EOF
