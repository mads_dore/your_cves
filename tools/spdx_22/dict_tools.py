# dict_tools.py
#
# This py file add various tools to manipulate dicts.
#
# Copyright (C) 2024 Mads Dore ApS, Denmark.

from debug_tools import debug_print

# ---------------------------------------------------------------------------------------------------------------------
# Make recursive updates of dictionary values for given keys. Returns number of updates made.
def dict_recursive_update(to_update, key, func, excluded_keys = [], debug = False, debug_level = None):
    
    updates = 0
            
    # Update if key exist in first level of a possibly nested dictionaries.
    if key in to_update: 
        if debug: debug_print(debug_level, "DO: "+to_update[key])
        func(to_update, key)
        updates += 1
        
        # formatted_work_dict = json.dumps(to_update, indent=2)
        # print(formatted_work_dict)
        
    # Check for possibly nested dictionaries.
    for k, v in to_update.items():
        if k in excluded_keys:
            continue
        elif isinstance(v,dict):
            # If nested dictionary make recursive call.
            if debug: debug_print(debug_level, "REC_DICT: "+k)
            updates += dict_recursive_update(v, key, func, excluded_keys, debug, debug_level+1)
        elif isinstance(v, list):
            # If list check if values are nested dictionaries.
            for lin, lv in enumerate(v):
                if isinstance(lv, dict):
                    if debug: debug_print(debug_level, "REC_LIST["+str(lin+1)+"/"+str(len(v))+"]: "+k)
                    updates += dict_recursive_update(lv, key, func, excluded_keys, debug, debug_level+1)

    return updates
    # LLOF
      
# ---------------------------------------------------------------------------------------------------------------------
# Make recursive removal of dictionary values by a given function. 
def dict_recursive_remove_by_func(to_remove, remove_func, debug = False, debug_level = None):

    for k, v in to_remove.copy().items():  
        if isinstance(v, list):
            # If list check if values are nested dictionaries.
            i = 0
            while i < len(v):
                if isinstance(v[i], dict):
                    remove_func(k, v[i])
                    
                    if not v[i]:
                        # Resulting dict is empty, remove completely from list.
                        v.pop(i)
                    else:    
                        # Part or all of the dict still exist, make recursive call as it might
                        # be nested.
                        if debug_level is not None:
                            used_debug_level = debug_level + 1
                        
                        dict_recursive_remove_by_func(v[i], remove_func, debug, None if debug_level is None else debug_level+1)
                        i = i + 1
                else:
                    i = i + 1
            
            if len(v) == 0:
                to_remove.pop(k)
                            
        elif isinstance(v,dict):
            remove_func(k, v)

            if not v:
                # Resulting dict is empty, remove key and value.
                to_remove.pop(k, None)
            else:    
                # Part or all of the dict still exist, make recursive call as it might
                # be nested.
            
                dict_recursive_remove_by_func(v, remove_func, debug, debug_level+1)
    # LLOF

# ---------------------------------------------------------------------------------------------------------------------
# Make recursive removal of dictionary values for given keys. 
def dict_recursive_remove_by_keys(to_remove, remove_keys = [], debug = False, debug_level = None):

    if debug: debug_print(debug_level, "DICT_REC_RM[IN]: "+str(remove_keys))
            
    for k, v in to_remove.copy().items():
        if k in remove_keys:
            if debug: debug_print(debug_level, "DICT_REC_RM: "+k)
            to_remove.pop(k, None)
        elif isinstance(v,dict):
            # If nested dictionary make recursive call.
            if debug: debug_print(debug_level, "DICT_REC_RM[REC-CALL]: "+k)
            dict_recursive_remove_by_keys(v, remove_keys, debug, None if debug_level is None else debug_level+1)
        elif isinstance(v, list):
            # If list check if values are nested dictionaries.
            for lin, lv in enumerate(v):
                if isinstance(lv, dict):
                    if debug: debug_print(debug_level, "REC_LIST_REM["+str(lin+1)+"/"+str(len(v))+"]: "+k)
                    dict_recursive_remove_by_keys(lv, remove_keys, debug, None if debug_level is None else debug_level+1)
    # LLOF
        
# ---------------------------------------------------------------------------------------------------------------------
# Make recursive extract of dictionary values for given keys. 
def dict_recursive_extract(to_extract, keys, func, excluded_keys = [], debug = False, debug_level = None):

    # Remove excluded keys from nested dict, before running func on it.
    dict_recursive_remove_by_keys(to_extract, excluded_keys, debug, debug_level+1)    
 
    if any(key in to_extract for key in keys):
        # At least one of the keys to extract is part of the dict, return the full dictionary after pop of excluded_keys.
        return func(to_extract, keys)
    
    # Test if end of nested dict is reached (Not a nested dict any more).    
    if not any(isinstance(item, dict) for item in to_extract.values()): 
        # None of the keys to extract are part of the dict, return None.
        return None
            
    # The dict must be nested when code ends up here, continue to iterate.
    return_dict = { }
    
    for k, v in to_extract.items():
        if isinstance(v,dict):
            # If nested dictionary make recursive call.
            if debug: debug_print(debug_level, "REC_DICT: "+k)
            extracted = dict_recursive_extract(v, keys, func, excluded_keys, debug, debug_level+1)
            if extracted is not None:
                # dict() to make a copy of the extracted dictionary, instead of parsing a reference.
                return_dict[k] = dict(extracted)                          
        elif isinstance(v, list):
            # If list check if values are nested dictionaries.
            return_list = []
            
            for lin, lv in enumerate(v):
                if isinstance(lv, dict):
                    if debug: debug_print(debug_level, "REC_LIST["+str(lin+1)+"/"+str(len(v))+"]: "+k)
                    extracted = dict_recursive_extract(lv, keys, func, excluded_keys, debug, debug_level+1)
                    if extracted is not None:
                        return_list.append(extracted)
            
            if (len(return_list) > 0):
                # [:] sliced notation to make a copy of the return_list, instead of parsing a reference.
                return_dict[k] = return_list[:]  
    
    if return_dict == {}:
        return_dict = None
 
    return return_dict
    # LLOF    
    
# EOF
