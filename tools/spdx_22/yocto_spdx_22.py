#!/usr/bin/env python3
# json_to_html.py
#
# This script is used to analyse Yocto SPDX 2.2 outputs, 
# to extract CPE information as well as SBOM information, 
# with some quality control informations.
#
# Copyright (C) 2024 Mads Doré ApS, Denmark.
#
# This source is released under the MIT License.
#
# DISCLAIMER
#
# This class/tool is meant to be used as support and not
# the only method to check against CVEs. Running this tool
# doesn't guarantee your packages are free of CVEs.

import getopt, sys, json
from your_cves_spdx import ycs_yocto_spdx_22_Error, ycs_yocto_spdx_22_API


# ====================================================================================================================
# Internal functions

def _analyse_relationships(relationships):

    read_recurse = [ ]

    for c in relationships:
        if c == 'c':
            read_recurse.append("CONTAINS")
        elif c == 'g':
            read_recurse.append("GENERATED_FROM")
        elif c == 'b':
            read_recurse.append("BUILD_DEPENDENCY_OF")
        elif c == 'r':
            read_recurse.append("RUNTIME_DEPENDENCY_OF")
        else:
            _print_help()
            sys.exit(2)
    
    return read_recurse

def _check_relationships(relationships):
    
    for relationship in relationships:
        if relationship not in ["CONTAINS", "GENERATED_FROM", "BUILD_DEPENDENCY_OF", "RUNTIME_DEPENDENCY_OF"]:
            _print_help()
            sys.exit(2)            

def _print_help():
    print("""
Usage: json_to_html [OPTION]... filename
Reads an Yocto SPDX 2.2 tar.zst file and analyses it.

  -h, --help             This help text.
  
  -f, --file             Yocto SPDX tar.zst file to analyse on.\n
  -m, --member           Root-member of the SPDX tar.zst to make recursive extract on.
  
  -p, --package          Package id (SPDXID) in the member to extract in.
  
  -s, --security         Extract security informations. CPEs, patch information etc.
                         CVEs are not included, as they are not in the SPDX 2.2. output.
                         
  -o, --overview         Overview of extracted packages as member[package_id].
  
  -c, --compressed_json  Print out compressed JSON for the member(s) in the argument
                             member or 
                             "[\'member_1\', \'member_2\', ...]"
                         Based on read of the member and package used in the extract.
                         
  -y, --yourcves         Your CVEs JSON file to test security extraction against.
                         Can be used for control purposes.
                         
  -r, --recurse          Relationships in the SPDX file to recurse through. Default = "cgbr".
                            c = "CONTAINS"
                            g = "GENERATED_FROM"
                            b = "BUILD_DEPENDENCY_OF"
                            r = "RUNTIME_DEPENDENCY_OF"
                         character shortform or list of full names.
                         
  -e, --extract          Relationships in the SPDX file to extract. Default = "cg".
                            c = "CONTAINS"
                            g = "GENERATED_FROM"
                            b = "BUILD_DEPENDENCY_OF"
                            r = "RUNTIME_DEPENDENCY_OF"
                         character shortform or list of full names.
                         
  -d, --debug            Print debug information.
  
Either --security or --packages, or both, must be set.""")

# ====================================================================================================================
# Python script execution

# -------- Read and analyse command line arguments -------
full_cmd_arguments  = sys.argv
argument_list       = full_cmd_arguments[1:]

short_options       = 'hse:f:m:p:yr:odc:'
long_options        = ['help', 'security', 'extract=', 'file=', 'member=', 'package=', 'yourcves=', 'recurse=', 'overview', 'debug','compressed_json=']

try:    
    options, remained = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    print(str(err))
    print("")
    _print_help()
    sys.exit(2)

option_file             = None
option_security         = False
option_overview         = False
option_debug            = False
option_extract          = "cg"
option_recurse          = "cgbr"
option_yourcves         = None
option_member           = None
option_package          = None
option_compressed_json  = None

for opt, arg in options:
    if opt in ('-h', '--help'):
        _print_help()
        sys.exit(2)
    elif opt in ('-f', '--file'):
        option_file = arg
    elif opt in ('-s', '--security'):
        option_security = True
    elif opt in ('-o', '--overview'):
        option_overview = True
    elif opt in ('-y', '--yourcves'):
        option_yourcves = arg
    elif opt in ('-r', '--recurse'):
        option_recurse  = arg
    elif opt in ('-e', '--extract'):
        option_extract  = arg
    elif opt in ('-d', '--debug'):
        option_debug    = True
    elif opt in ('-m', '--member'):
        option_member   = arg
    elif opt in ('-p', '--package'):
        option_package  = arg
    elif opt in ('-c', '--compressed_json'):
        option_compressed_json  = arg

if (not (option_security or option_overview)) or \
   (option_member is None) or \
   (option_file is None) or \
   (option_package is None):
    _print_help()
    sys.exit(2)

if option_recurse is None:
    # Set defaults.
    option_recurse = ["CONTAINS", "GENERATED_FROM" ]
else:
    if '[' in option_recurse:
        from ast import literal_eval
        
        option_recurse = literal_eval(option_recurse)
        _check_relationships(option_recurse)
    elif len(option_recurse) > 0:
        option_recurse = _analyse_relationships(option_recurse)
    else:
        _print_help()
        sys.exit(2)

if option_extract is None:
    # Set defaults.
    option_extract = ["CONTAINS", "GENERATED_FROM", "BUILD_DEPENDENCY_OF", "RUNTIME_DEPENDENCY_OF"]
else:
    if '[' in option_extract:
        from ast import literal_eval
        
        option_extract = literal_eval(option_extract)
        _check_relationships(option_extract)
    elif len(option_recurse) > 0:
        option_extract = _analyse_relationships(option_extract)
    else:
        _print_help()
        sys.exit(2)

if option_compressed_json is not None:
    if '[' in option_compressed_json:
        from ast import literal_eval
        
        option_compressed_json = literal_eval(option_compressed_json)
    elif len(option_compressed_json) > 0:
        option_compressed_json = [ option_compressed_json ]
    else:
        _print_help()
        sys.exit(2)

# print the analysed command line, to help if there are errors or odd results.

analysed_cl = "yocto_spdx_22.py"

if (option_file is not None) and (len(option_file)>0):
    analysed_cl += " --file=\"%s\"" % option_file

if (option_member is not None) and (len(option_member)>0):
    analysed_cl += " --member=\"%s\"" % option_member

if (option_package is not None) and (len(option_package)>0):
    analysed_cl += " --package=\"%s\"" % option_package

if option_security:
    analysed_cl += " -s"

if option_overview:
    analysed_cl += " -o"

if option_yourcves is not None:
    analysed_cl += " --yourcves=\"%s\"" % option_yourcves

analysed_cl += " --recurse=\"%s\"" % str(option_recurse)
analysed_cl += " --extract=\"%s\"" % str(option_extract)

if option_compressed_json is not None:
    analysed_cl += " --compressed_json=\"%s\"" % str(option_compressed_json)

if option_debug:
    analysed_cl += " -d"
print("---- Analysed command line ----")
print(analysed_cl)

# -------- Execute the commands -------

# SPDX tar.zst file initial setup.
spdx = ycs_yocto_spdx_22_API(option_file)

member_reference = spdx.lookup_index_member(option_member)
name_space       = member_reference["documentNamespace"]

if option_security:
    extract_security = [] 
    spdx.extract_package_relationships_recursive(name_space, 
                                                 option_package,
                                                 option_recurse,
                                                 option_extract,
                                                 extract_security,
                                                 extract_function = spdx.from_package_extract_SECURITY, 
                                                 debug = option_debug, 
                                                 debug_level = 0)

    if option_yourcves is not None:
        print("---- Your CVEs JSON file comparison ----")
        spdx.test_against_cve_file(extract_security, option_yourcves)

if option_overview:
    extract_overview = {}

    spdx.extract_package_relationships_recursive(name_space, 
                                                 option_package,
                                                 option_recurse,
                                                 option_extract,
                                                 extract_overview,
                                                 debug = option_debug, 
                                                 debug_level = 0)    

    print("\r\n---- Extract overview ----")
    lines_not_read = []
    lines_read     = []
    
    
    for name_space in extract_overview:
        reference   = spdx.lookup_index_reference(name_space)
        member      = reference["filename"]

        if (name_space in spdx.storage["extracted"]):
            read_info = "read    "
            lines_list = lines_read
        else:
            read_info = "NOT READ"      
            lines_list = lines_not_read
        
        max_len_member      = spdx.storage["print_setup"]["max_len_filename"]
        format_string       = read_info+" %-"+str(max_len_member)+"s"
        line_to_add         = format_string % member
        len_spacing         = len(line_to_add)-21+len(read_info)
        format_string = "%s"
        for project_id in extract_overview[name_space]:
            lines_list.append(line_to_add+(format_string % project_id))
            format_string = "           ↳ "+(' '*len_spacing)+"%s"
            line_to_add   = ""
    
    i = 1
    for line in lines_not_read:
        print("%4s %s" % (str(i), line))
        i = i + 1
    for line in lines_read:
        print("%4s %s" % (str(i), line))
        i = i + 1
    
if option_compressed_json is not None:
    
    print("\r\n---- Compressed JSON ----")
    
    for member in option_compressed_json:
    
        try:
            compressed_member_reference = spdx.lookup_index_member(member)
        except ycs_yocto_spdx_22_Error as err:
            print(err)
            continue
            
        compressed_name_space       = compressed_member_reference["documentNamespace"]
        
        if compressed_name_space not in spdx.storage["extracted"]:
            print("Member (%s) not read during extraction." % member)
        else:
            print(member)
            print(json.dumps(spdx.storage["extracted"][compressed_name_space], indent=4)) 

exit()

if option_yourcves is not None:

    with open(option_yourcves) as json_file:
        test_json = json.load(json_file)

        combined = []

        for recipe in test_json["your_cves_1_0"]["recipes"]:
            recipe_exists = False
            for test_item in combined:
                if (test_item["recipe"] == recipe["recipe"]) and \
                   (test_item["version"] == recipe["version"]):
                    recipe_exists = True
                    break
            
            if recipe_exists:
                break
                      
            new_item = {}
            new_item["recipe"]            = recipe["recipe"]
            new_item["version"]           = recipe["version"]
            new_item["cve_file_cpes"]     = []
        
            for cpe in recipe["cpes"]:
                new_item["cve_file_cpes"].append("cpe:2.3:*:"+cpe["cpe"].replace('%','*')+":*:*:*:*:*:*:*")

            combined.append(new_item)
    
    for recipe in extracted:
        i = 0
        while i < len(combined):
            if (recipe["recipe"] == combined[i]["recipe"]) and \
               (recipe["version"] == combined[i]["version"]):
               break
            i = i + 1
        
        if i < len(combined):
            combined[i]["spdx_cpes"]      = []
            for cpe in recipe["cpes"]:
                combined[i]["spdx_cpes"].append(cpe["cpe"])
        else:
            new_item = {}
            new_item["recipe"]            = recipe["recipe"]
            new_item["version"]           = recipe["version"]
            new_item["spdx_cpes"]         = []
            if cpe in recipe["cpes"]:
                new_item["spdx_cpes"]         = cpe["cpe"]
            
            combined.append(new_item)
  
    count_recipe = 1
    
    print("%3s %-30s %-20s %-4s %-4s %-60s %-60s" % ("#", "recipe", "version", "cve", "spdx", "cve[cpe]", "spdx[cpe]"))
    for recipe in combined:
        test_keys   = {}
        test_index  = {}
        test_ids    = ["spdx_cpes", "cve_file_cpes"]
        
        for key in test_ids:
            test_keys[key]    = False
            test_index[key]   = 0
            
            if key in recipe:
                recipe[key].sort()
                test_keys[key] = True
        
        spdx_found  = "-"
        cve_found   = "-"
        if test_keys["spdx_cpes"]:
            spdx_found = "OK"
        if test_keys["cve_file_cpes"]:
            cve_found = "OK"
         
        print("%3s %-30s %-20s %-4s %-4s " % (str(count_recipe), recipe["recipe"], recipe["version"], cve_found, spdx_found), end='')
        
        first_run = True
        
        while True:
            lines_left = False
            for key in test_ids:
                if test_keys[key]:
                    if test_index[key] < len(recipe[key]):
                        lines_left = True
            if not lines_left:
                break
            
            cve_files_cpe = ""
            if test_keys["cve_file_cpes"]:
                if test_index["cve_file_cpes"] < len(recipe["cve_file_cpes"]):
                    cve_files_cpe = recipe["cve_file_cpes"][test_index["cve_file_cpes"]]
                    test_index["cve_file_cpes"] = test_index["cve_file_cpes"] + 1
            
            spdx_cpe = ""
            if test_keys["spdx_cpes"]:
                if test_index["spdx_cpes"] < len(recipe["spdx_cpes"]):
                    if cve_files_cpe != "":
                        if cve_files_cpe == recipe["spdx_cpes"][test_index["spdx_cpes"]]:
                            spdx_cpe = cve_files_cpe
                            test_index["spdx_cpes"] = test_index["spdx_cpes"] + 1
                        else: 
                            spdx_cpe = "-"
                    else:
                        cve_files_cpe = "-"
                        spdx_cpe = recipe["spdx_cpes"][test_index["spdx_cpes"]]
                        test_index["spdx_cpes"] = test_index["spdx_cpes"] + 1
        
            if not first_run:
                print((' '*66), end='')
            first_run = False
                    
            print("%-60s %-60s" % (cve_files_cpe, spdx_cpe))
        
        if first_run:
            print("")
        print('-'*187)
        count_recipe = count_recipe + 1
    # print(json.dumps({ "Test result" : combined}, indent=4))    
                              
    # LLOF

if False:
    spdx.test_full_analysis()

    exit()

# EOF
