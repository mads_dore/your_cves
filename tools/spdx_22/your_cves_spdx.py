# your_cves_spdx.py
#
# This py file is used to analyse SPDX 2.2 files from Yocto.
#
# Copyright (C) 2024 Mads Dore ApS, Denmark.

from dict_tools import dict_recursive_remove_by_keys
from debug_tools import debug_print
from pathlib import Path
import tarfile
import tempfile
import json
import zstandard  

# #####################################################################################################################
# EXTERNAL EXPOSED CLASSES

# ---------------------------------------------------------------------------------------------------------------------
# your CVSs specific exception class.
class ycs_yocto_spdx_22_Error(Exception):
    pass

    # LLOC

# ---------------------------------------------------------------------------------------------------------------------
# API for your_cves yocto SPDX 2.2 interface.
class ycs_yocto_spdx_22_API:

    # ================================================ Private methods ================================================
    
    # -----------------------------------------------------------------------------------------------------------------
    # Initialization of class for reading Yocto SPDX tar.zst files.
    #
    # archive:          Path to tar.zst file with SPDX files from Yocto. 
    def __init__(self, archive: Path):

        self.archive = Path(archive).expanduser()
    
        if zstandard is None:
            raise ycs_yocto_spdx_22_Error("pip install zstandard")

        dctx = zstandard.ZstdDecompressor()

        self.temp_output_file = tempfile.TemporaryFile(suffix=".tar")
        with self.archive.open("rb") as input_file:
            dctx.copy_stream(input_file, self.temp_output_file)
        self.temp_output_file.seek(0)
        
        # Get the names of files in the tar, for later control handling.
        self.temp_tarfile = tarfile.open(fileobj=self.temp_output_file)
        self.member_names = self.temp_tarfile.getnames()
        self.member_names.sort()
        
        # Get the index for file look-up later.
        self.storage = {}
        self.storage["index"] = self.__get_member_dict("index.json")
        
        self.storage["print_setup"] = { 
            "max_len_name_space" : 0,
            "max_len_filename" : 0}
            
        for document in self.storage["index"]["documents"]:
            self.storage["print_setup"]["max_len_name_space"] = max(
                    self.storage["print_setup"]["max_len_name_space"],
                    len(document["documentNamespace"]))
            self.storage["print_setup"]["max_len_filename"] = max(
                    self.storage["print_setup"]["max_len_filename"],
                    len(document["filename"]))
     
        # LLOF
        
    # -----------------------------------------------------------------------------------------------------------------
    # Class destructor.
    def __del__(self):
    
        self.temp_tarfile.close()
        self.temp_output_file.close()
        # LLOF
        
    # -----------------------------------------------------------------------------------------------------------------
    # Read a SPDX file from the tar.zst file initiated.
    #
    # member_name:      Name of the SPDX file in the tar.zst to read.
    # check_index:      Check if member_name is in the index file, throw an exception if not (Default = True).
    #
    # return:           Read SPDX dictionary. 
    def __get_member_dict(self, member_name, check_index = True):
    
        if check_index:
            if not member_name in self.member_names:
                raise ycs_yocto_spdx_22_Error(member_name + " is not in archive.")
        member_file = self.temp_tarfile.extractfile(member_name)  
        member_dict = json.load(member_file)
        return member_dict
        # LLOF

  # -----------------------------------------------------------------------------------------------------------------
    # Get full SPDX document, based on its namespace in the read index file.
    #
    #   name_space:     Namespace to extract, 
    #                   Example: "http://spdx.org/spdxdoc/kernel-6.5.10-66cec2da-8022-5a07-bfc1-acc720ae1eee"
    #   storage_id:     Key of the storage in self.storage for storing SPDX document, will be auto-generated. 
    #                   Will be stored in self.storage[storage_id][name_space]
    #
    #   return:         Extracted dictionary from the SPDC file, after it was stored in self.storage[storage_id].
    def __get_full_spdx_document(self, name_space, storage_id):
    
        # Create storage if it does not exist.
        if not storage_id in self.storage:
            self.storage[storage_id] = {}
       
        if name_space not in self.storage[storage_id]:
            reference = self.lookup_index_reference(name_space)
            work_dict = self.__get_member_dict(reference["filename"])
            
            self.storage[storage_id][name_space] = work_dict 
            
        return self.storage[storage_id][name_space]
        # LLOF   

    # -----------------------------------------------------------------------------------------------------------------
    # Reads and reduces parts of a SPDX document, based on its namespace.
    #
    #   reduction_func: The reduction function to run when reading the SPDX dict.
    #   name_space:     Namespace to extract, if None all name_spaces in self.storage["index"] is extracted 
    #                   and reduced.
    #                   Example: "http://spdx.org/spdxdoc/kernel-6.5.10-66cec2da-8022-5a07-bfc1-acc720ae1eee".
    #   storage_id:     Key of the storage in self.storage for storing the extractions, will be auto-generated. 
    #                   Will be stored in self.storage[storage_id][name_space]
    #   debug:          Shall debug be printed or not.
    #   debug_level:    Indentation level of the debug print.
    #
    #   return:         Totals statistics from the reduction function.
    def __read_and_reduce(self, reduction_func = None, name_space = None, storage_id = "extracted", 
                              debug = False, debug_level = 0):

        # --- local method ---
        def spdx_read_and_reduce(total, local_name_space):
            
            if storage_id in self.storage:
                if local_name_space in self.storage[storage_id]:
                    # Already read.
                    return total
                    
            self.__get_full_spdx_document(local_name_space, storage_id)
            if debug: debug_print(debug_level, "Read and reduce: "+local_name_space)
            
            if reduction_func is not None:
                total = reduction_func(total, local_name_space, storage_id, debug, debug_level+1)
                
            return total
             
            # LLOLM            

        # --- local main ---
        totals = None
        
        if name_space is None:        
            for reference in self.storage["index"]["documents"]:
                totals = spdx_read_and_reduce(totals, reference["documentNamespace"])
        else:
            totals = spdx_read_and_reduce(totals, name_space)
        
        return totals
        # LLOF

    # -----------------------------------------------------------------------------------------------------------------
    # Recursive run through a linked list of SPDX documents and AMENDS document relations with type OTHER.  
    #                       if [name_space][package_id]["compacted_relationship"]["OTHER"] -> 
    #                           for related_name_space in "OTHER":
    #                               amend all AMENDS in related_name_space with type "RUNTIME_DEPENDENCY_OF"
    #
    #   name_space:     Namespace to run AMENDS on.
    #                   Example: "http://spdx.org/spdxdoc/kernel-6.5.10-66cec2da-8022-5a07-bfc1-acc720ae1eee".
    #   package_id:     Package_id to run AMENDS on.
    #   storage_id:     Key of the storage in self.storage for storing the extractions, will be auto-generated. 
    #                   Will be stored in self.storage[storage_id][name_space]
    #   debug:          Shall debug be printed or not.
    #   debug_level:    Indentation level of the debug print.
    #
    #   return:         Totals statistics from the reduction function.
    def __other_amends_recursive(self, name_space, package_id, storage_id = "extracted", debug = False, debug_level = 0):

        # --- local method ---
        def amends(name_space):
        
            count  = -1
            
            if not ("additional_relationships" in self.storage[storage_id][name_space]):
                return count
            
            i = 0
            
            while i < len(self.storage[storage_id][name_space]["additional_relationships"]):
                if self.storage[storage_id][name_space]["additional_relationships"][i]["relationshipType"] == "AMENDS":
                    break
                else:
                    i = i + 1
            
            if i == len(self.storage[storage_id][name_space]["additional_relationships"]):
                return count
                
            amends_info = self.storage[storage_id][name_space]["additional_relationships"][i]
            amends_related = amends_info["relatedSpdxElement"].split(':')
            
            if (len(amends_related) != 2) or \
               (amends_related[1] != "SPDXRef-DOCUMENT") or \
               (amends_info["spdxElementId"] != "SPDXRef-DOCUMENT"):
                # Wrongly typed AMENDS as to what we compare in this function.
                return count
        
            count   = 0
            i       = 0
            
            while i < len(self.storage[storage_id][name_space]["additional_relationships"]):
            
                relation = self.storage[storage_id][name_space]["additional_relationships"][i]
               
                if relation["relationshipType"] == "RUNTIME_DEPENDENCY_OF":
                   
                    assert(amends_related[0] == relation["relatedSpdxElement"].split(':')[0]), "AMENDS define not within analysis range of the implementation!"
                   
                    matched_relationship    = self.__match_package_reference(name_space, relation["relatedSpdxElement"], storage_id)
                    dependency_relationship = self.__match_package_reference(name_space, relation["spdxElementId"], storage_id)
                    
                    assert(matched_relationship is not None), "Matched is None: "+str(relation["relatedSpdxElement"])
                    
                    package_info = self.get_package(matched_relationship["name_space"], matched_relationship["package_id"], 
                                                    storage_id, debug, debug_level + 1)
                    
                    assert(package_info is not None), "Package not found: "+str(matched_relationship)
                    
                    if "compacted_relationships" not in package_info:
                        package_info["compacted_relationships"] = {}
                    
                    if relation["relationshipType"] not in package_info["compacted_relationships"]:
                        package_info["compacted_relationships"][relation["relationshipType"]] = []
                    
                    package_info["compacted_relationships"][relation["relationshipType"]].append(dependency_relationship)
                    
                    if debug: debug_print(debug_level+1, "AMENDS: "+matched_relationship["package_id"]+"."+relation["relationshipType"]+" append "+str(dependency_relationship)+"")
                    count = count + 1
                                      
                    self.storage[storage_id][name_space]["additional_relationships"].pop(i)  
                else:
                    i = i + 1           
         
            if len(self.storage[storage_id][name_space]["additional_relationships"]) == 1:
                assert(amends_info == self.storage[storage_id][name_space]["additional_relationships"][0]), "AMENDS is not the last element in list, as otherwise expected."
                
                self.storage[storage_id][name_space].pop("additional_relationships")
                
                if "packages" not in self.storage[storage_id][name_space]:
                    # Neither packages nor additional_relationships in compacted SPDX document any more, externalDocumentsRefs can be removed.
                    self.storage[storage_id][name_space].pop("externalDocumentRefs")
            
            return count

        # --- main method ---

        count = 0
        
        if (name_space is None) or (package_id is None):
            return None

        package = self.get_package(name_space, package_id, storage_id = storage_id, debug = debug, debug_level = debug_level+1)

        if "compacted_relationships" in package:
            if "OTHER" in package["compacted_relationships"].keys():
                if debug: debug_print(debug_level, "Recursive AMENDS from OTHER: "+name_space+"["+package_id+"]")
                i =  0
                while i < len(package["compacted_relationships"]["OTHER"]):
                    relation = package["compacted_relationships"]["OTHER"][i]
                    matched_relationship = self.__match_package_reference(name_space, relation, storage_id)
                    
                    assert(matched_relationship is not None), "Matched is None: "+str(relation)
                    assert(matched_relationship["package_id"] == "SPDXRef-DOCUMENT"), "Package is not SPDXRef-DOCUMENT: "+str(relation)

                    if matched_relationship["name_space"] not in self.storage[storage_id]:
                        self.__read_and_reduce(self.__yocto_spdx22_reduction, matched_relationship["name_space"], storage_id, debug = debug, debug_level = debug_level+1)                        
                    
                    amends_return = amends(matched_relationship["name_space"])
                    if (amends_return >= 0):
                        package["compacted_relationships"]["OTHER"].pop(i)
                        count = count + amends_return
                    else:
                        i = i + 1
                        
                if len(package["compacted_relationships"]["OTHER"]) == 0:
                    package["compacted_relationships"].pop("OTHER")
                    
                if "RUNTIME_DEPENDENCY_OF" in package:
                    for dependency in package["RUNTIME_DEPENDENCY_OF"]:
                        count = count + self.__other_amends_recursive(dependency["name_space"], dependency["package_id"], 
                                                                      storage_id, debug, debug_level+1)
        return count
        # LLOF

    # -----------------------------------------------------------------------------------------------------------------
    # Reducing parts of a extracted SPDX document, based on its namespace, according to SPDX 2.2 output from Yocto.
    # Used to reduce the total data-set to speed up later calculations and extractions.
    #
    #   totals:         Statistics, where None will generate the right structure and a already existing structure will
    #                   be the base whereto reduction statistics are added. New total is returned at end of the 
    #                   function execution.        
    #   name_space:     Namespace to reduce, 
    #                   Example: "http://spdx.org/spdxdoc/kernel-6.5.10-66cec2da-8022-5a07-bfc1-acc720ae1eee"
    #   storage_id:     Key of the storage in self.storage where that already extraced SPDX dicts are stored.
    #                   Assertion will be made, if name_space does not exist in the expected location.
    #   debug:          Shall debug be printed or not.
    #   debug_level:    Indentation level of the debug print.
    #
    #   return:         Updated total statistics.
    def __yocto_spdx22_reduction(self, totals = None, name_space = None, storage_id = "extracted", debug = False, debug_level = 0):
    
        # --- local method ---
        def spdx_compact_files(name_space):
            
            files_compacted     = 0
                
            if "files" in self.storage[storage_id][name_space]:
            
                files_to_compact    = len(self.storage[storage_id][name_space]["files"])
                files_shown_count   = 0
                files_shown_deliter = max(1, int(files_to_compact/100))
                if debug: debug_print(debug_level, "Compacting files: 0% of "+str(files_to_compact)+" done.", print_end='\033[K\r')
            
                compacted_files = {}
                    
                for file_info in self.storage[storage_id][name_space]["files"]:
                    compacted_file_info = {}
                    
                    if file_info["copyrightText"] != "NOASSERTION":
                        compacted_file_info["copyrightText"] = file_info["copyrightText"]

                    if file_info["licenseConcluded"] != "NOASSERTION":
                        compacted_file_info["licenseConcluded"] = file_info["licenseConcluded"]

                    if (len(file_info["licenseInfoInFiles"]) > 1) or \
                        (file_info["licenseInfoInFiles"][0] != "NOASSERTION"):
                            compacted_file_info["licenseInfoInFiles"] = file_info["licenseInfoInFiles"]

                    assert(file_info["SPDXID"] not in compacted_file_info), "SPDXID "+file_info["SPDXID"]+" already in compacted_files."
                    
                    compacted_files[file_info["SPDXID"]] = compacted_file_info
                   
                    files_compacted = files_compacted + 1
                    if ((files_compacted - files_shown_count) > files_shown_deliter):
                        if debug: debug_print(debug_level, "Compacting files: "+str(int(100*files_compacted/files_to_compact))+"% of "+str(files_to_compact)+" done.", print_end='\r\033[K')
                     
                self.storage[storage_id][name_space]["compacted_files"] = compacted_files
                self.storage[storage_id][name_space].pop("files")
            
            return files_compacted
            # LLOLM            
    
        # --- local method ---
        def spdx_combine_relationships_with_compacted_files(name_space):
            
            relations_compacted     = 0
                
            if "relationships" in self.storage[storage_id][name_space]:

                add_relationships   = []
               
                relations_to_compact    = len(self.storage[storage_id][name_space]["relationships"])
                    
                if "compacted_files" in self.storage[storage_id][name_space]:
                
                    relations_shown_count   = 0
                    relations_shown_deliter = max(1, int(relations_to_compact/100))
                    if debug: debug_print(debug_level, "Compacting relationships: 0% of "+str(relations_to_compact)+" done.", print_end='\033[K\r')
                
                    compacted_files = self.storage[storage_id][name_space]["compacted_files"]
                    
                    for relationship in self.storage[storage_id][name_space]["relationships"]:
                    
                        if relationship["spdxElementId"] in compacted_files:
                                            
                            related_type        = relationship["relationshipType"]
                            spdx_element        = relationship["spdxElementId"]
                            related_elements    = relationship["relatedSpdxElement"].split(':')
                            related_element_id  = related_elements[0]
                            
                            if len(related_elements) > 1:
                                related_element_subid = related_elements[1]
                            else:
                                related_element_subid = None
                                        
                            if related_type == "GENERATED_FROM":
                                if (related_element_id == "NOASSERTION") and ("comment" in relationship):
                                    related_element_subid = relationship["comment"]    
                           
                            if related_type not in compacted_files[spdx_element]:
                                compacted_files[spdx_element][related_type] = {}
                            
                            if related_element_id not in compacted_files[spdx_element][related_type]:
                                compacted_files[spdx_element][related_type][related_element_id] = []
                                
                            assert(related_element_subid is not None), "Compacted files reduction does not include case of subid = None."
                            if related_element_subid not in compacted_files[spdx_element][related_type][related_element_id]:
                                compacted_files[spdx_element][related_type][related_element_id].append(related_element_subid)
                        else:
                            add_relationships.append(relationship)
                
                        relations_compacted = relations_compacted + 1
                    
                    if ((relations_compacted - relations_shown_count) > relations_shown_deliter):
                        if debug: debug_print(debug_level, "Compacting relationships: "+str(int(100*relations_compacted/relations_to_compact))+"% of "+str(relations_to_compact)+" done.", print_end='\r')
               
                else:
                    add_relationships = self.storage[storage_id][name_space]["relationships"] 
            
                    relations_compacted = relations_to_compact 
               
                self.storage[storage_id][name_space]["additional_relationships"] = add_relationships
                self.storage[storage_id][name_space].pop("relationships")
  
            return relations_compacted
            # LLOLM     
            
        # --- local method ---
        def spdx_packages_with_files_and_relationships(name_space):
            
            packages_combined = 0
            
            if "packages" in self.storage[storage_id][name_space] and \
               "additional_relationships" in self.storage[storage_id][name_space]:
            
                packages_to_combine    = len(self.storage[storage_id][name_space]["packages"])
                packages_shown_count   = 0
                packages_shown_deliter = max(1, int(packages_to_combine/100))
                if debug: debug_print(debug_level, "Combining packages: 0% of "+str(packages_to_combine)+" done.", print_end='\033[K\r')
            
                for package_info in self.storage[storage_id][name_space]["packages"]:
                    SPDXID = package_info["SPDXID"]
                    
                    i = 0
                    while i < len(self.storage[storage_id][name_space]["additional_relationships"]):
                       
                        relationship = self.storage[storage_id][name_space]["additional_relationships"][i] 
                        
                        if relationship["relatedSpdxElement"] == SPDXID: 
                            if (relationship["relationshipType"] == "DESCRIBES") and \
                               (relationship["spdxElementId"] == "SPDXRef-DOCUMENT"):
                        
                                self.storage[storage_id][name_space]["additional_relationships"].pop(i)
                            else:
                                if "compacted_relationships" not in package_info:
                                    package_info["compacted_relationships"] = {}
                                
                                if relationship["relationshipType"] not in package_info["compacted_relationships"]:
                                    package_info["compacted_relationships"][relationship["relationshipType"]] = []
                                
                                package_info["compacted_relationships"][relationship["relationshipType"]].append(relationship["spdxElementId"])
                                self.storage[storage_id][name_space]["additional_relationships"].pop(i)     
                        
                        elif (relationship["spdxElementId"] == SPDXID) and \
                             ("hasFiles" in package_info) and \
                             (relationship["relationshipType"] == "CONTAINS"):

                            if relationship["relatedSpdxElement"] in package_info["hasFiles"]:
                                
                                if "compacted_files" not in package_info:
                                    package_info["compacted_files"] = {}
                                
                                package_info["compacted_files"][relationship["relatedSpdxElement"]] = \
                                    self.storage[storage_id][name_space]["compacted_files"][relationship["relatedSpdxElement"]] 
                            
                                self.storage[storage_id][name_space]["compacted_files"].pop(relationship["relatedSpdxElement"])
                                self.storage[storage_id][name_space]["additional_relationships"].pop(i)     
                                
                                package_info["hasFiles"].remove(relationship["relatedSpdxElement"])
                            else:
                                i = i + 1         
                                               
                        elif relationship["spdxElementId"] == SPDXID:

                            if "compacted_relationships" not in package_info:
                                package_info["compacted_relationships"] = {}
                            
                            if relationship["relationshipType"] not in package_info["compacted_relationships"]:
                                package_info["compacted_relationships"][relationship["relationshipType"]] = []
                            
                            package_info["compacted_relationships"][relationship["relationshipType"]].append(relationship["relatedSpdxElement"])
                            
                            self.storage[storage_id][name_space]["additional_relationships"].pop(i)     
                                    
                        else:
                            i = i + 1
           
                    if "hasFiles" in package_info:
                        if len(package_info["hasFiles"]) == 0:
                            package_info.pop("hasFiles")    

                    packages_combined = packages_combined + 1
                    if ((packages_combined - packages_shown_count) > packages_shown_deliter):
                        if debug: debug_print(debug_level, "Combining packages: "+str(int(100*packages_combined/packages_to_combine))+"% of "+str(packages_to_combine)+" done.", print_end='\r')
            
            if "additional_relationships" in self.storage[storage_id][name_space]:
                if len(self.storage[storage_id][name_space]["additional_relationships"]) == 0:
                    self.storage[storage_id][name_space].pop("additional_relationships")

            if "compacted_files" in self.storage[storage_id][name_space]:
                if len(self.storage[storage_id][name_space]["compacted_files"]) == 0:
                    self.storage[storage_id][name_space].pop("compacted_files")     
        
            return packages_combined                       
            # LLOLM            
        
        # --- local method ---
        def spdx_clean_up(name_space):
           
            to_clean = self.storage[storage_id][name_space]
        
            to_clean.pop("SPDXID")
            to_clean.pop("creationInfo")
            to_clean.pop("dataLicense")
            to_clean.pop("spdxVersion")
            
            dict_recursive_remove_by_keys(to_clean, 
                                          ["checksum", "checksums", "packageVerificationCode"])
            # LLOLM

        # --- local method ---
        def spdx_packages_other_amends(name_space):
            
            count = 0
            if "packages" in self.storage[storage_id][name_space]:
                for package_info in self.storage[storage_id][name_space]["packages"]:
                    count = count + self.__other_amends_recursive(name_space, package_info["SPDXID"], 
                                                                  storage_id, debug, debug_level)
            
            return count
      
        # --- local method ---
        def statistics(total = None, addition = None):
            if total is None:
                total = {
                    "files": 0,
                    "relations": 0,
                    "packages": 0,
                    "amends": 0
                }
            
            if addition is not None:
                for key in total:
                    total[key] = total[key] + addition[key] 
             
            return total
            # LLOLM 
             
        # --- local method ---
        def combined(total, name_space):
            if debug: debug_print(debug_level, "YOCTO SPDX22 reduction: "+name_space)
            f_add = spdx_compact_files(name_space)
            r_add = spdx_combine_relationships_with_compacted_files(name_space)
            p_add = spdx_packages_with_files_and_relationships(name_space)
            spdx_clean_up(name_space)
            a_add = spdx_packages_other_amends(name_space)
            
            return statistics(total, {
                "files"     : f_add,
                "relations" : r_add,
                "packages"  : p_add,
                "amends"    : a_add
            })       
            # LLOLM
                    
        # --- main method ---
        
        if name_space is None:        
            for reference in self.storage["index"]["documents"]:
                totals = combined(totals, reference["documentNamespace"])
        else:
            totals = combined(totals, name_space)
        
        return totals
        # LLOF

    # -----------------------------------------------------------------------------------------------------------------
    # Matching a local reference to a name_space for a given package reference.
    #
    #   name_space:     Name space containing the reference information.
    #                   Example: "http://spdx.org/spdxdoc/kernel-6.5.10-66cec2da-8022-5a07-bfc1-acc720ae1eee"
    #   reference:      The reference to match.
    #   storage_id:     Key of the storage in self.storage where that already extraced SPDX dicts are stored.
    #   debug:          Shall debug be printed or not.
    #   debug_level:    Indentation level of the debug print.
    #
    #   return:         None if reference was empty or not in self.storage[storage_id] otherwise:
    #                       { name_space: "<name_space information",
    #                         package_id: "<package_id>"}
    def __match_package_reference(self, name_space, reference, storage_id, debug = False, debug_level = 0):

        ref_elements    = reference.split(':')
        
        if len(ref_elements) > 1:
            ext_package_id = ref_elements[1]
        elif len(ref_elements) == 1:
            return { "name_space": name_space, "package_id": ref_elements[0] }   
        else:
            raise ycs_yocto_spdx_22_Error("Reference was empty.")
        
        if (name_space not in self.storage[storage_id]): 
            raise ycs_yocto_spdx_22_Error("Name space matched not found in storage.")
        
        ext_doc_id      = ref_elements[0]
        ext_name_space  = None

        if "externalDocumentRefs" in self.storage[storage_id][name_space]:
            for reference in self.storage[storage_id][name_space]["externalDocumentRefs"]:
                if reference["externalDocumentId"] == ext_doc_id:
                    ext_name_space = reference["spdxDocument"]
                    
        if ext_name_space is None:
            raise ycs_yocto_spdx_22_Error("External name space reference not found in spdx document.")
        
        return { "name_space": ext_name_space, "package_id": ext_package_id }
  
    # ================================================ Public methods  ================================================

    # -----------------------------------------------------------------------------------------------------------------
    # Reads and reduces all SPDX documents referenced in storage["index"]["documents"].
    #
    #   storage_id:     Key of the storage in self.storage for storing the extractions, will be auto-generated. 
    #                   Will be stored in self.storage[storage_id][name_space]
    #   debug:          Shall debug be printed or not.
    #   debug_level:    Indentation level of the debug print.
    #
    #   return:         Totals statistics from the reduction function.
    def read_and_reduce(self, storage_id = "extracted", debug = False, debug_level = 0):

        return self.__read_and_reduce(self.__yocto_spdx22_reduction, None, storage_id, debug, debug_level)            
        # LLOF
  
    # -----------------------------------------------------------------------------------------------------------------
    # Print all member names, as found in tar-file.
    #
    #   print_flush:   Flush setting for the print, default is false.
    def print_member_names(self, print_flush = False):
   
        for mn in self.member_names:
            print(mn, flush = print_flush)
        # LLOF
        
    # -----------------------------------------------------------------------------------------------------------------
    # Print raw SPDX dict based on a given member name from tar-file.
    #
    #   member_name:   Member name of the SPDX dict to print.
    #                  E.g. "runtime-zip.spdx.json"
    #   print_indent:  Indent for the json.dumps function used for the print, default is 2.
    #   print_flush:   Flush setting for the print, default is false.
    def print_member_dict(self, member_name, print_indent = 2, print_flush = False):
    
        print(json.dumps(self.__get_member_dict(member_name), indent = print_indent), flush = print_flush)
        # LLOF
    
    def get_package(self, name_space, package_id, storage_id = "extracted", debug = False, debug_level = 0):
        
        if (storage_id not in self.storage) or \
           (name_space not in self.storage[storage_id]): 
            self.__read_and_reduce(self.__yocto_spdx22_reduction, name_space, storage_id, debug = debug, debug_level = debug_level)
                
        if name_space in self.storage[storage_id]:
            if "packages" in self.storage[storage_id][name_space]:
                for package in self.storage[storage_id][name_space]["packages"]:
                    if package_id == package["SPDXID"]:
                        return package 

        return None
        # LLOF

    # -----------------------------------------------------------------------------------------------------------------
    # Extracts relationships from a package(s)
    #
    #   package:       Package to extract from.
    #   extract:       List of relationship type to extract from the package.
    #   output:        Dict to add to extractions to. Structure is:
    #                       { name_space: [<package_ids>]}
    #   name_space:    Name space from there the package was read, needed to allow extraction of relationship 
    #                  information.
    #   storage_id:    Key of the storage in self.storage where that already extraced SPDX dicts are stored.
    def extract_package_relationships(self, package, extract, output, name_space, storage_id = "extracted"):

        if "compacted_relationships" in package:
            for relationship_type in extract:
                if relationship_type in package["compacted_relationships"]:
                    for relation in package["compacted_relationships"][relationship_type]:

                        if type(relation) is dict:
                            matched_relationship = relation
                        else:
                            matched_relationship = self.__match_package_reference(name_space, relation, storage_id)
                        
                        if matched_relationship["name_space"] not in output:
                            output[matched_relationship["name_space"]] = []
                        
                        if matched_relationship["package_id"] not in output[matched_relationship["name_space"]]:
                            output[matched_relationship["name_space"]].append(matched_relationship["package_id"]) 
                        else:       
                            continue
        return True                 
                     
    def extract_package_relationships_recursive(self, name_space, package_id, recurse_trough, extract, output,
                                                      storage_id = "extracted",
                                                      extract_function = None,  
                                                      call_paths = None, debug = False, debug_level = 0):

        def recursive_calls(relationship_type, package, debug_level):

            call_path_string = name_space+"["+package["SPDXID"]+"]"
            if call_path_string in call_paths["reextract_check"]:
                if debug: debug_print(debug_level, "Stop recursion on reextract: "+call_path_string)
                return 
            
            call_paths["reextract_check"].append(call_path_string)

            if relationship_type in package:
                for relation_type in package[relationship_type].keys():
                    if relation_type in recurse_trough:
                        for relation in package[relationship_type][relation_type]:
                            if ("name_space" in relation) and ("package_id" in relation):
                                matched_relationship = relation
                            else:
                                matched_relationship = self.__match_package_reference(name_space, relation, storage_id)
                            
                            assert(matched_relationship is not None), "Matched is None: "+str(relation)
                            
                            call_path_string = matched_relationship["name_space"]+"["+matched_relationship["package_id"]+"]"
                            if call_path_string in call_paths["recursive_check"]:
                                # Cyclic relationship, drop the recursions.
                                if debug: debug_print(debug_level, "Stop recursion on relation: "+call_path_string)
                                continue
                            
                            call_paths["recursive_check"].append(call_path_string)
                                
                            self.extract_package_relationships_recursive(matched_relationship["name_space"], 
                                                                         matched_relationship["package_id"], 
                                                                         recurse_trough,
                                                                         extract, 
                                                                         output,
                                                                         storage_id,
                                                                         extract_function,
                                                                         call_paths, 
                                                                         debug, debug_level + 1)
            
                            call_paths["recursive_check"].remove(call_path_string)
        
        if (name_space is None) or (package_id is None):
            raise ycs_yocto_spdx_22_Error("Name space or package id is None.")

        if (call_paths == None):
            call_paths = {
                "recursive_check" : [], 
                "reextract_check" : []
            }    
            
        if debug: debug_print(debug_level, "Extract package relationships recursive: "+name_space+"["+package_id+"]")
        
        package = self.get_package(name_space, package_id, storage_id = storage_id, debug = debug, debug_level = debug_level+1)
       
        if extract_function is None:
            self.extract_package_relationships(package, extract, output, name_space, storage_id)
        else:
            extract_function(package, extract, output, name_space, storage_id)
        
        recursive_calls("compacted_relationships", package, debug_level+1)
        # if debug: debug_print(debug_level, "Stop recursion on extract: "+name_space+package["SPDXID"])
                                 
        # LLOF
            
    def from_package_extract_SECURITY(self, package, extract, output, name_space, debug = False, debug_level = 0):

        def append_cpe_to_output(output_index, recipe, version, cpe):
            
            if output_index < 0:
                i = 0
            else:
                i = output_index
            
            while i < len(output):
                if (output[i]["recipe"] == recipe) and (output[i]["version"] == version):
                    break
                i = i + 1
            
            
            if i < len(output):
                # Recipe already exists in extracted, add cpe to the existing entry.
                for test_cpe in output[i]["cpes"]:
                    if test_cpe["cpe"] == cpe:
                        return i
                output[i]["cpes"].append({ "cpe" : cpe})
                
            else:
                # Recipe must be created in extracted.
                new_item = {}
                new_item["recipe"]      = recipe
                new_item["version"]     = version.replace('+git','')
                new_item["products"]    = [ recipe ]
                new_item["cpes"]        = [ { "cpe" : cpe }]
   
                output.append(new_item)
            
            return i

        def append_sourceinfo_to_output(output_index, patched, ignored):
            
            assert (output_index < len(output)), "Index error."
          
            # Recipe already exists in extracted, add cpe to the existing entry.
            output[output_index]["patched"] = patched
            output[output_index]["ignored"] = ignored
                
        if "externalRefs" in package:
            work_index = -1
            for ext_ref in package["externalRefs"]:
                if ext_ref["referenceCategory"] == "SECURITY":
                    if ext_ref["referenceLocator"] not in output:
                        work_index = append_cpe_to_output(work_index, package["name"], package["versionInfo"],ext_ref["referenceLocator"])

            if work_index > -1:
                patched = []
                ignored = []
                if "sourceInfo" in package:
                    for info_line in package["sourceInfo"].split('\n'):
                        if info_line.startswith("CVEs fixed: "):
                            for cve_id in info_line.replace("CVEs fixed: ", "").split(" "):
                                if cve_id not in patched:
                                    patched.append(cve_id) 

                        if info_line.startswith("CVEs ignored: "):
                            for cve_id in info_line.replace("CVEs ignored: ", "").split(" "):
                                if cve_id not in ignored:
                                    ignored.append(cve_id) 

                append_sourceinfo_to_output(work_index, patched, ignored)    
        # LLOF
        
    def extract_from_packages(self, packages, extract_function, output,
                                    storage_id = "extracted", 
                                    debug = False, debug_level = 0):
      
        if debug: debug_print(debug_level, "Extract from packages.")
        
        for name_space in packages:
            for package_id in packages[name_space]:
                print(name_space+" - "+package_id)
                package = self.get_package(name_space, 
                                           package_id, 
                                           storage_id, 
                                           debug, debug_level+1)
       
                extract_function(package, output, debug, debug_level+1)
        
        # LLOF
 
    def extract_package_info_recursive(self, name_space, package_id, extract_relationships, extract_function, extract_data,
                                       storage_id = "extracted", 
                                       call_paths = {"recursive_check" : [], "reextract_check" : []}, 
                                       debug = False, debug_level = 0):
                                       
        def recursive_calls(relationship_type, package, debug_level):
            if relationship_type in package:
                for relation_type in package[relationship_type].keys():
                    if relation_type in extract_relationships:
                        for relation in package[relationship_type][relation_type]:
                            if ("name_space" in relation) and ("package_id" in relation):
                                matched_relationship = relation
                            else:
                                matched_relationship = self.__match_package_reference(name_space, relation, storage_id)
                            
                            assert(matched_relationship is not None), "Matched is None: "+str(relation)
                            
                            call_path_string = matched_relationship["name_space"]+"["+matched_relationship["package_id"]+"]"
                            if call_path_string in call_paths["recursive_check"]:
                                # Cyclic relationship, drop the recursions in this relation.
                                if debug: debug_print(debug_level, "Stop recursion on info: "+call_path_string)
                                continue
                            
                            call_paths["recursive_check"].append(call_path_string)
                            
                            self.extract_package_info_recursive(matched_relationship["name_space"], matched_relationship["package_id"], extract_relationships, extract_function, extract_data,
                                           storage_id, call_paths, debug = debug, debug_level = debug_level + 1)
            
                            call_paths["recursive_check"].remove(call_path_string)
                            
        if (name_space is None) or (package_id is None):
            return None

        if debug: debug_print(debug_level, "Extract package info recursive: "+name_space+"["+package_id+"]")
        
        package = self.get_package(name_space, package_id, storage_id = storage_id, debug = debug, debug_level = debug_level+1)
       
        extract_function(package, extract_data, name_space, storage_id, call_paths)
        
        recursive_calls("compacted_relationships", package, debug_level + 1)
        
        # LLOF
 

    def test_full_analysis(self, test_set = ["AMENDS_VS_PACKAGES"], debug = True, debug_level = 0):
        
        name_space = None
        
        print(self.read_and_reduce("full_test", True))
        
        import json, sys
        
        for test in test_set:
            
            if test == "AMENDS_VS_PACKAGES":
                # TEST PURPOSE: There shall be EITHER packages or AMENDS in an SPDX document.
                if debug: debug_print(debug_level, "Test full analysis = AMENDS_VS_PACKAGES")
                debug_level = debug_level + 1

                count_ok  = 0
                count_nok = 0
                
                files_counts = {
                    "packages"  : 0,
                    "amends"    : 0,
                    "empty"     : 0    
                }

                for name_space in self.storage["full_test"]:
                    spdx_doc = self.storage["full_test"][name_space]
                    
                    has_packages = False
                    has_amends   = False
                    
                    if "packages" in spdx_doc:                        
                        if len(spdx_doc["packages"]) > 0:
                            has_packages = True
                            files_counts["packages"] = files_counts["packages"] + 1
                   
                    if "additional_relationships" in spdx_doc:
                        count_amends = 0
                        
                        for relationship in spdx_doc["additional_relationships"]:
                            if relationship["relationshipType"] == "AMENDS":
                                count_amends = count_amends + 1
                               
                                if (relationship["spdxElementId"] != "SPDXRef-DOCUMENT") or \
                                   (relationship["relatedSpdxElement"].split(':')[1] != "SPDXRef-DOCUMENT"):
                                    # TEST SUB-PURPOSE: Check if there are AMENDS the references to anything else than a 
                                    #                   full SPDX document, ID = "SPDXRef-DOCUMENT".
                                    if debug: debug_print(debug_level, "INFO - None SPDXRef-DOCUMENT AMENDS in %s." % name_space) 
                        
                        if count_amends > 0:
                            has_amends = True
                            files_counts["amends"] = files_counts["amends"] + 1
                            
                            if count_amends > 1:
                                # TEST SUB-PURPOSE: Check if there are more than ONE AMENDS in a SPDX document.
                                if debug: debug_print(debug_level, "INFO - %s AMENDS in %s. %" % (str(count_amends), name_space)) 
                            
                            if len(spdx_doc["additional_relationships"]) > 1:
                                # TEST SUB-PURPOSE: Check if there are more additional relationships then AMENDS in the SPDX document.
                                if debug: debug_print(debug_level, "INFO - AMENDS and %s additional_relationships in %s." % (str(len(spdx_doc["additional_relationships"])), name_space)) 
                                
                    if (has_amends == True) and (has_packages == True):
                        # TEST PURPOSE: Both packages and AMENDS are NOT allowed.
                        if debug: debug_print(debug_level, "NOT OK - Analysis AMENDS_VS_PACKAGES]: %s has both AMENDS and packages!" % name_space)   
                        count_nok = count_nok + 1
                    elif (has_amends == False) and (has_packages == False):
                        # TEST PURPOSE: Neither packages nor AMENDS are NOT allowed.
                        if (len(spdx_doc.keys()) == 2) and \
                           ("documentNamespace" in spdx_doc) and \
                           ("name" in spdx_doc):
                            # Everything read and compressed out of SPDX document, all OK.
                            count_ok = count_ok + 1    
                            files_counts["empty"] = files_counts["empty"] + 1
                        else:
                            count_nok = count_nok + 1
                            if debug: debug_print(debug_level, "NOT OK - Analysis AMENDS_VS_PACKAGES]: %s has neither AMENDS nor packages!" % name_space)
                    else:
                        count_ok = count_ok + 1

                if count_nok == 0:
                    if debug: debug_print(debug_level, "OK - Analysis AMENDS_VS_PACKAGES]: %s SPDX's has either packages, AMENDS or is empty %s !" % (str(count_ok), str(files_counts)))
                else:    
                    if debug: debug_print(debug_level, "NOT OK - Analysis AMENDS_VS_PACKAGES]: %s SPDX's has either AMENDS or packages and %s has errors!" % (str(count_ok), str(count_nok)))

                debug_level = debug_level - 1

        # LLOF
        
    def test_against_cve_file(self, extracted, cve_file, debug = True, debug_level = 0):
        import json
        
        with open(cve_file) as json_file:
            test_json = json.load(json_file)

            combined = []

            for recipe in test_json["your_cves_1_0"]["recipes"]:
                recipe_exists = False
                for test_item in combined:
                    if (test_item["recipe"] == recipe["recipe"]) and \
                       (test_item["version"] == recipe["version"]):
                        recipe_exists = True
                        break
                
                if recipe_exists:
                    break
                          
                new_item = {}
                new_item["recipe"]            = recipe["recipe"]
                new_item["version"]           = recipe["version"]
                new_item["cve_file_cpes"]     = []
                new_item["cve_patched"]       = []
                new_item["cve_whitelisted"]   = []
            
                for cpe in recipe["cpes"]:
                    cpe_to_insert = "cpe:2.3:*:"+cpe["cpe"].replace('%','*')+":*:*:*:*:*:*:*"
                    if cpe_to_insert not in new_item["cve_file_cpes"]: 
                        new_item["cve_file_cpes"].append(cpe_to_insert)
                        
                    for patched in cpe["patched"]:
                        new_item["cve_patched"].append({ "ID" : patched["ID"], "comment" : patched["comment"]})

                    for whitelisted in cpe["whitelisted"]:
                        new_item["cve_whitelisted"].append({ "ID" : whitelisted["ID"], "comment" : whitelisted["comment"]})
                                  
                for cpe in recipe["additional_cpes"]:
                    cpe_to_insert = "cpe:2.3:*:"+cpe.replace('%','*')+":*:*:*:*:*:*:*"
                    if cpe_to_insert not in new_item["cve_file_cpes"]: 
                        new_item["cve_file_cpes"].append(cpe_to_insert)
        
                if isinstance(recipe["patched_no_cve"], dict):
                    search_list = recipe["patched_no_cve"].items()
                else:
                    search_list = recipe["patched_no_cve"]
                
                for cve_id, comment in search_list:
                    new_item["cve_patched"].append({ "ID" : cve_id, "comment" : comment})

                if isinstance(recipe["whitelisted_no_cve"], dict):
                    search_list = recipe["whitelisted_no_cve"].items()
                else:
                    search_list = recipe["whitelisted_no_cve"]
                
                for cve_id, comment in search_list:
                    new_item["cve_whitelisted"].append({ "ID" : cve_id, "comment" : comment})

                combined.append(new_item)
        
        for recipe in extracted:
            i = 0            
            while i < len(combined):
                if (recipe["recipe"] == combined[i]["recipe"]) and \
                   (recipe["version"] == combined[i]["version"]):
                   break
                i = i + 1
            
            if i < len(combined):
                combined[i]["spdx_patched"]   = recipe["patched"]
                combined[i]["spdx_ignored"]   = recipe["ignored"]
                combined[i]["spdx_cpes"]      = []
                for cpe in recipe["cpes"]:
                    combined[i]["spdx_cpes"].append(cpe["cpe"])
            else:
                new_item = {}
                new_item["recipe"]            = recipe["recipe"]
                new_item["version"]           = recipe["version"]
                new_item["spdx_patched"]      = recipe["patched"]
                new_item["spdx_ignored"]      = recipe["ignored"]
                new_item["spdx_cpes"]         = []
                if cpe in recipe["cpes"]:
                    new_item["spdx_cpes"]         = cpe["cpe"]
                
                combined.append(new_item)
      
        count_recipe = 1
        
        print("%3s %-30s %-20s %-4s %-4s %-60s %-60s %-20s %-20s %-20s %-20s" % ("#", "recipe", "version", "cve", "spdx", "cve[cpe]", "spdx[cpe]", "cve[patched]", "spdx[patched]", "cve[whitelisted]", "spdx[ignored]"))
        for recipe in combined:
            test_keys   = {}
            test_index  = {}
            test_ids    = ["spdx_cpes", "cve_file_cpes", "cve_patched", "spdx_patched", "cve_whitelisted", "spdx_ignored"]
            
            for key in test_ids:
                test_keys[key]    = False
                test_index[key]   = 0
                
                if key in recipe:
                    if len(recipe[key]) > 0:
                        if isinstance(recipe[key][0], dict):
                            new_list = sorted(recipe[key], key=lambda d: d['ID'])
                            recipe[key] = new_list
                        else:
                            recipe[key].sort()
                  
                    test_keys[key] = True
            
            spdx_found  = "-"
            cve_found   = "-"
            if test_keys["spdx_cpes"]:
                spdx_found = "OK"
            if test_keys["cve_file_cpes"]:
                cve_found = "OK"
             
            print("%3s %-30s %-20s %-4s %-4s " % (str(count_recipe), recipe["recipe"], recipe["version"], cve_found, spdx_found), end='')
            
            first_run = True
            
            while True:
                lines_left = False
                for key in test_ids:
                    if test_keys[key]:
                        if test_index[key] < len(recipe[key]):
                            lines_left = True
                if not lines_left:
                    break
                
                cve_files_cpe = ""
                if test_keys["cve_file_cpes"]:
                    if test_index["cve_file_cpes"] < len(recipe["cve_file_cpes"]):
                        cve_files_cpe = recipe["cve_file_cpes"][test_index["cve_file_cpes"]]
                        test_index["cve_file_cpes"] = test_index["cve_file_cpes"] + 1
                
                spdx_cpe = ""
                if test_keys["spdx_cpes"]:
                    if test_index["spdx_cpes"] < len(recipe["spdx_cpes"]):
                        if cve_files_cpe != "":
                            if cve_files_cpe == recipe["spdx_cpes"][test_index["spdx_cpes"]]:
                                spdx_cpe = cve_files_cpe
                                test_index["spdx_cpes"] = test_index["spdx_cpes"] + 1
                            else: 
                                spdx_cpe = "-"
                        else:
                            cve_files_cpe = "-"
                            spdx_cpe = recipe["spdx_cpes"][test_index["spdx_cpes"]]
                            test_index["spdx_cpes"] = test_index["spdx_cpes"] + 1

                cve_patched = ""
                if test_keys["cve_patched"]:
                    if test_index["cve_patched"] < len(recipe["cve_patched"]):
                        cve_patched = recipe["cve_patched"][test_index["cve_patched"]]["ID"]
                        test_index["cve_patched"] = test_index["cve_patched"] + 1

                spdx_patched = ""
                if test_keys["spdx_patched"]:
                    if test_index["spdx_patched"] < len(recipe["spdx_patched"]):
                        if cve_patched != "":
                            if cve_patched == recipe["spdx_patched"][test_index["spdx_patched"]]:
                                spdx_patched = cve_patched
                                test_index["spdx_patched"] = test_index["spdx_patched"] + 1
                            else: 
                                spdx_patched = "-"
                        else:
                            cve_patched = "-"
                            spdx_patched = recipe["spdx_patched"][test_index["spdx_patched"]]
                            test_index["spdx_patched"] = test_index["spdx_patched"] + 1


                cve_whitelisted = ""
                if test_keys["cve_whitelisted"]:
                    if test_index["cve_whitelisted"] < len(recipe["cve_whitelisted"]):
                        cve_whitelisted = recipe["cve_whitelisted"][test_index["cve_whitelisted"]]["ID"]
                        test_index["cve_whitelisted"] = test_index["cve_whitelisted"] + 1
 
                spdx_ignored = ""
                if test_keys["spdx_ignored"]:
                    if test_index["spdx_ignored"] < len(recipe["spdx_ignored"]):
                        if cve_whitelisted != "":
                            if cve_whitelisted == recipe["spdx_ignored"][test_index["spdx_ignored"]]:
                                spdx_ignored = cve_whitelisted
                                test_index["spdx_ignored"] = test_index["spdx_ignored"] + 1
                            else: 
                                spdx_ignored = "-"
                        else:
                            cve_whitelisted = "-"
                            spdx_ignored = recipe["spdx_ignored"][test_index["spdx_ignored"]]
                            test_index["spdx_ignored"] = test_index["spdx_ignored"] + 1
           
                if not first_run:
                    print((' '*66), end='')
                first_run = False
               
                print("%-60s %-60s %-20s %-20s %-20s %-20s" % (cve_files_cpe, spdx_cpe, cve_patched, spdx_patched, cve_whitelisted, spdx_ignored))
            
            if first_run:
                print("")
            print('-'*271)
            count_recipe = count_recipe + 1
        # print(json.dumps({ "Test result" : combined}, indent=4))    
                                  
        # LLOF

    def not_func(self, package, dummy_1, name_space, storage_id, call_paths):
        
        call_path_string = name_space+"["+package["SPDXID"]+"]"
        if call_path_string in call_paths["reextract_check"]:
            return False
        
        call_paths["reextract_check"].append(call_path_string)
        return True


    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    def yocto_spdx22_empty(self, name_space = None, storage_id = "extracted", debug = False, debug_level = 0):
    
        if name_space is None:
            return None
        
        if self.storage[storage_id] not in None:
            return None
        
        return ((len(self.storage[storage_id][name_space].keys()) == 2) and \
                ("documentNamespace" in self.storage[storage_id][name_space]) and \
                ("name" in self.storage[storage_id][name_space]))
    


    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
    # -----------------------------------------------------------------------------------------------------------------
    # Look-up of member name_space in index.json of the tar.zst file. Exception thrown if not found or multiple 
    # results found.
    #
    # member_name:      Member name to look-up, 
    #                   Example: "http://spdx.org/spdxdoc/attr-b5afe69f-446c-5b4a-81f6-2ec4d1763025"
    #
    # return:           Look-up result dict. 
    #                   Example: { 'documentNamespace': 'http://spdx.org/spdxdoc/attr-b5afe69f-446c-5b4a-81f6-2ec4d1763025', 
    #                              'filename':          'attr.spdx.json', 
    #                              'sha1':              'c5b0ef62e6a28b6a2affa22da308cbd67595f1b8'
    #                            }
    def lookup_index_member(self, member_name):
        result = list(filter(lambda document: (document["filename"] == member_name), self.storage["index"]["documents"]))
        
        if len(result) == 0:
            raise ycs_yocto_spdx_22_Error(member_name + " is not in index.json.")
        elif len(result) == 1:
            return result[0]
            
        raise ycs_yocto_spdx_22_Error(member_name + " in index.json multiple times.")  
 
        # LLOM
        
    # -----------------------------------------------------------------------------------------------------------------
    # Look-up of document reference in index.json of the tar.zst file. Exception thrown if not found or multiple 
    # results found.
    #
    # name_space:       Namespace to look-up, 
    #                   Example: "http://spdx.org/spdxdoc/attr-b5afe69f-446c-5b4a-81f6-2ec4d1763025"
    #
    # return:           Look-up result dict. 
    #                   Example: { 'documentNamespace': 'http://spdx.org/spdxdoc/attr-b5afe69f-446c-5b4a-81f6-2ec4d1763025', 
    #                              'filename':          'attr.spdx.json', 
    #                              'sha1':              'c5b0ef62e6a28b6a2affa22da308cbd67595f1b8'
    #                            }
    def lookup_index_reference(self, name_space):
        result = list(filter(lambda document: (document["documentNamespace"] == name_space), self.storage["index"]["documents"]))
        
        if len(result) == 0:
            raise ycs_yocto_spdx_22_Error(name_space + " is not in index.json.")
        elif len(result) == 1:
            return result[0]
            
        raise ycs_yocto_spdx_22_Error(name_space + " in index.json multiple times.")
        
        # LLOM      
                
    # LLOC
# EOF
