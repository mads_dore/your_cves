#!/usr/bin/env python3
# yoct_json_to_server_json.py
#
# This script is used to generate a JSON file for 
# automatic alert on your-cves-server detached from
# Yocto without Yocto CVE QA included.
#
# Copyright (C) 2020 Mads Doré ApS, Denmark.
#
# This source is released under the MIT License.
#
# DISCLAIMER
#
# This class/tool is meant to be used as support and not
# the only method to check against CVEs. Running this tool
# doesn't guarantee your packages are free of CVEs.

import json
import jsonref
import getopt, sys
from os.path import join, dirname, basename,getmtime
from pprint import pprint, pformat
from datetime import datetime

# CMD line options

full_cmd_arguments  = sys.argv
argument_list       = full_cmd_arguments[1:]

short_options       = 'h'
long_options        = ['help']

def _print_help():
    print('Usage: yocto_json_to_server_json [OPTION]... filename')
    print('Reads a your_cves JSON file from Yocto and converts to a generelt your-cves-server JSON installation description.')
    print('')
    print('  -h, --help             This  help text.')

try:    
    options, remained = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    print(str(err))
    print("")
    _print_help()
    sys.exit(2)

option_cve             = False
option_reduced         = False

for opt, arg in options:
    if opt in ('-h', '--help'):
        _print_help()
        sys.exit(2)

# Get JSON files and generaly need informations
YC_json_file = join(dirname(__file__), remained[0])

with open(YC_json_file) as json_file:
    YC_data = json.load(json_file)

if not YC_data['your_cves_1_0']:
    print("ERROR no your_cves_1_0 dictionary found in JSON file.")
    quit()


YC_server_data = { 'your_cves_server_1_0': { 'groups': []  } }

for recipe in YC_data['your_cves_1_0']['recipes']:

    product_group = { 'name': recipe['recipe'], 
                      'version': recipe['version'], 
                      'products': [] }

    for cpe in recipe['cpes']:
        cpe_to_append = { 'cpe': cpe['cpe'], 'whitelisted': [], 'patched': [] }

        for whitelisted in cpe['whitelisted']:
            whitelist_to_append = { 'ID': whitelisted['ID'], 'comment': whitelisted['comment'] }
            cpe_to_append['whitelisted'].append(whitelist_to_append)

        for patched in cpe['patched']:
            patch_to_append = { 'ID': patched['ID'], 'comment': patched['comment'] }
            cpe_to_append['patched'].append(patch_to_append)

        product_group['products'].append(cpe_to_append)
    
    YC_server_data['your_cves_server_1_0']['groups'].append(product_group)
    
print(json.dumps(YC_server_data))

quit()

# EOF
