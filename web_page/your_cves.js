/* 
# your_cves.js
#
# This javascript file is used to generate a web-server 
# for Yocto CVE QA and CVE overviews, with your CVEs 
# JSON files as input (generated in Yocto with 
# your_cves_checks).
#
# Copyright (C) 2020 Mads Doré ApS, Denmark.
#
# This source is released under the MIT License.
#
# DISCLAIMER
#
# This class/tool is meant to be used as support and not
# the only method to check against CVEs. Running this tool
# doesn't guarantee your packages are free of CVEs. */

var reading_in_progress = false;
var include_not_vulnerable = true;

var score_totals_reset = {
  'patched': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }},
  'unpatched': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }},
  'whitelisted': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }},
  'not_vulnerable': { "cves": {}, "counts" : { "none": 0, "low": 0, "medium": 0, "high": 0, "critical": 0 }}
}

var score_totals = $.extend( true, {}, score_totals_reset);

function escapeHTML(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
}

function checkDoubleCVEs(group, cve_id, vendor_product, version) {
  
  var check_value = vendor_product + ":" + version;

  if (cve_id in window.score_totals[group]["cves"]) {
    if (window.score_totals[group]["cves"][cve_id].includes(check_value)) {
      return false;
    }
    window.score_totals[group]["cves"][cve_id].push(check_value);    
  }
  else {
    window.score_totals[group]["cves"][cve_id] = [ check_value ];
  }  

  return true;
}

function addScoreToGroup(score_totals) {

  var selector = "";

  $.each(window.score_totals, function (group, info) {
    var total = 0;

    $.each(info["counts"], function (cvss_3_class, count_in_class) {
      
      total += count_in_class;

      if (count_in_class > 0) {
        var class_to_add  = 'cvss_V3_severity_'+cvss_3_class;

        selector      = '#CVE_group_' + group + ' .cve_group_v3_' + cvss_3_class;
        
        $(selector).addClass(class_to_add);

        $(selector).html(count_in_class);
      }
    })

    selector = '#CVE_group_' + group + ' .cve_group_total';

    $(selector).html(total);
    if (total > 0) {
      $(selector).addClass("status_"+group);            
      if (total > 5) {
        selector = '#CVE_group_' + group + ' .cve_sortline';
        
        $(selector).show();
      }
    }

  })
}

function readJSONfile(url) {
  var your_cves_json_read = false;
  var error_message       = "";
  var general_info        = "";
    
  if (window.reading_in_progress) return;
  window.reading_in_progress = true;
  
  $('#recipeList').html("");
  $('#cveGroupList').html("");
  window.score_totals = $.extend( true, {}, window.score_totals_reset);
  
  var ts = new Date().getTime();

  url += "?_="+ts;

  $.getJSON(url, function (data) {
    $.each(data, function (key, your_cves_array) {
      if (key == "your_cves_1_0") {
        if (!your_cves_json_read) {
          general_info = your_cves_array["configuration"]["sort_out_not_vulnerable"];           

          window.include_not_vulnerable = (general_info == "");
          if (!window.include_not_vulnerable) {    
            $('div.total_unpatched').css({ width: "33.333333%", width: "calc(100%/3)" });
            $('div.total_unpatched_sort').css({ width: "33.333333%", width: "calc(100%/3)" });
            $('div.total_patched').css({ width: "33.333333%", width: "calc(100%/3)" });
            $('div.total_patched_sort').css({ width: "33.333333%", width: "calc(100%/3)" });
            $('div.total_whitelisted').css({ width: "33.333333%", width: "calc(100%/3)" });
            $('div.total_whitelisted_sort').css({ width: "33.333333%", width: "calc(100%/3)" });
            $('div.total_not_vulnerable').css({ display: "none" });
            $('div.total_not_vulnerable_sort').css({ display: "none" });
          }  

          var cveGroupItem = $('<li class="cve_group_li">' + $('#listCVEGroupItem').html() + '</li>');

          $('.cve_group_status', cveGroupItem).html("Unpatched")
          $('.cve_group_total', cveGroupItem).html("0")
          cveGroupItem.attr('id', "CVE_group_unpatched");
          $('#cveGroupList').append(cveGroupItem.clone());   

          $('.cve_group_status', cveGroupItem).html("Whitelisted")
          cveGroupItem.attr('id', "CVE_group_whitelisted");
          $('#cveGroupList').append(cveGroupItem.clone());   

          $('.cve_group_status', cveGroupItem).html("Patched")
          cveGroupItem.attr('id', "CVE_group_patched");
          $('#cveGroupList').append(cveGroupItem.clone());   

          if (window.include_not_vulnerable) {
            $('.cve_group_status', cveGroupItem).html("Not_vulnerable")
            cveGroupItem.attr('id', "CVE_group_not_vulnerable");
            $('#cveGroupList').append(cveGroupItem.clone()); 
          }

          your_cves_array["recipes"].forEach(extractRecipe);    

          addScoreToGroup();

          your_cves_json_read = true;
        }
        else {
          error_message = "ERROR multiple your_cves dictionaries in JSON file.";
          $('.general_info').html(error_message); 
          window.reading_in_progress = false;
          return;
        }
      }
    })

    if (!your_cves_json_read) {
      error_message = "ERROR no your_cves_1_0 dictionary found in JSON file.";
    }
    else {
      your_cves_sort_recipe(score_totals);
      $('.file_loader').hide();        
      $('.view_selector').show();        
    }

    if (error_message != "") {
        $('.general_info').html(error_message); 
    }
    else {
        $('.general_info').text(general_info);           
    }
    window.reading_in_progress = false;
  })
  .error(function() { $('.file_loading_info').html("ERROR when trying to read file."); 
                      window.reading_in_progress = false; })
}

function extractRecipe(recipe, index) {
  var recipeItem = $('<li class="recipe_li">' + $('#listRecipeItem').html() + '</li>');

  /* Make Yocto CVE Quality Assurance information. */
  var yocto_cve_qa                = "NOT IMPLEMENTED";
  var yocto_cve_qa_eval_style     = "";
  var yocto_cve_qa_info           = "";
  var yocto_cve_qa_tracking_info  = "";

  yocto_cve_qa_tracking_info += 
    '<p class="small_heading">Yocto CVE QA tracking:</p>';

  if ((recipe.cve_qa.evaluated_id === "") && (recipe.cve_qa.evaluated_date === "")) {
    yocto_cve_qa_tracking_info += "No evaluation made.</br>";
    yocto_cve_qa = "NO EVAL";
    yocto_cve_qa_eval_style = "yocto_qa_no_valid_eval";
  }
  else if ((recipe.cve_qa.evaluated_id === "") || (recipe.cve_qa.evaluated_date === "")) {
      yocto_cve_qa_tracking_info += "Wrong evaluation note.</br>";
      yocto_cve_qa = "EVAL ERROR";
      yocto_cve_qa_eval_style = "yocto_qa_no_valid_eval";
  }
  else {
    yocto_cve_qa_info += "Evaluation done by "+recipe.cve_qa.evaluated_id+" on "+recipe.cve_qa.evaluated_date+".</br>";
    yocto_cve_qa = "EVAL DONE";

    if ((recipe.cve_qa.reviewed_id === "") && (recipe.cve_qa.reviewed_date === "")) {
      yocto_cve_qa_tracking_info += "No review made.</br>";
      yocto_cve_qa = "NO REVIEW";
      yocto_cve_qa_eval_style = "yocto_qa_no_valid_review";
    }
    else if ((recipe.cve_qa.reviewed_id === "") || (recipe.cve_qa.reviewed_date === "")) {
      yocto_cve_qa_tracking_info += "Wrong review note.</br>";
      yocto_cve_qa = "REVIEW ERROR";
      yocto_cve_qa_eval_style = "yocto_qa_no_valid_review";
    }
    else {
      yocto_cve_qa_tracking_info += "Review done by "+recipe.cve_qa.review_id+" on "+recipe.cve_qa.review_date+".</br>";
      yocto_cve_qa = "OK";        
      yocto_cve_qa_eval_style = "yocto_qa_ok";
    }    
  }
  
  if ((!jQuery.isEmptyObject(recipe.patched_no_cve)) ||
      (!jQuery.isEmptyObject(recipe.whitelisted_no_cve))) {

    /* Override on purpose */
    yocto_cve_qa            = "CVE errors";
    yocto_cve_qa_eval_style = "yocto_qa_cve_errors";

    if (!jQuery.isEmptyObject(recipe.patched_no_cve)) {
      yocto_cve_qa_info += 
        '<p class="small_heading">CVEs marked patched with no matching ID for CVE or CPE in NITS NVD:</p>';

      $.each(recipe.patched_no_cve, function (key, comment) {
        yocto_cve_qa_info += key+": "+comment+"</br>";
      })
    }

    if (!jQuery.isEmptyObject(recipe.whitelisted_no_cve)) {
      yocto_cve_qa_info += 
        '<p class="small_heading">CVEs whitelisted with no matching ID for CVE or CPE in NITS NVD:</p>';

      $.each(recipe.whitelisted_no_cve, function (key, comment) {
        yocto_cve_qa_info += key+": "+comment+"</br>";
      })
    }
  }

  /* Make Yocto CPE information. */
  var cpe_info = "";

  if (!jQuery.isEmptyObject(recipe.products)) {
    cpe_info += '<p class="small_heading">Recipe products used as CPEs:</p>';
    cpe_info += '<span class="info_lead">Common version:</span> '+escapeHTML(recipe.version)+'<br/>';
    cpe_info += '<span class="info_lead">Products:</span> '+escapeHTML(recipe.products.join(' , '));
  }
  
  if (!jQuery.isEmptyObject(recipe.addition_cpes)) {
    cpe_info += '<p class="small_heading">Additional CPEs:</p>';
    cpe_info += '<span class="info_lead">CPEs:</span> '+escapeHTML(recipe.additional_cpes.join(' , '));
  }

  var comments = ""

  if (typeof recipe.skipped.products !== 'undefined') {
    comments += "Skipped: "+escapeHTML(recipe.skipped.products);
  }  
  if (typeof recipe.skipped.recipe !== 'undefined') {
    comments += (comments != "") ? "</br>" : "";
    comments += "Skipped: "+escapeHTML(recipe.skipped.recipe);
  }  
  if (typeof recipe.skipped.all_cpes !== 'undefined') {
    comments += (comments != "") ? "</br>" : "";
    comments += "Skipped: "+escapeHTML(recipe.skipped.all_cpes);
  }  

  $('.recipe', recipeItem).html(recipe.recipe);
  $('.version', recipeItem).html(recipe.version);
  $('.comments', recipeItem).html(comments);      
  $('.yocto_qa_comments', recipeItem).html(yocto_cve_qa_tracking_info);   

  $('.total_unpatched', recipeItem).html(recipe.stats.unpatched);
  $('.total_whitelisted', recipeItem).html(recipe.stats.whitelisted);
  $('.total_not_vulnerable', recipeItem).html(recipe.stats.unpatched_not_vulnerable);
  $('.total_patched', recipeItem).html(recipe.stats.patched);
  
  if (recipe.stats.unpatched > 0) { $('.total_unpatched', recipeItem).addClass("status_unpatched") }
  if (recipe.stats.patched > 0) { $('.total_patched', recipeItem).addClass("status_patched") }
  if (recipe.stats.whitelisted > 0) { $('.total_whitelisted', recipeItem).addClass("status_whitelisted") }
  if (recipe.stats.unpatched_not_vulnerable > 0) { $('.total_not_vulnerable', recipeItem).addClass("status_not_vulnerable") }

  $('.yocto_cve_qa', recipeItem).html(yocto_cve_qa);
  if (yocto_cve_qa_eval_style != "") {
    $('.yocto_cve_qa', recipeItem).addClass(yocto_cve_qa_eval_style);
  }

  $('.recipe_cpes', recipeItem).html(cpe_info);  
  $('.recipe_cve_qa', recipeItem).html(yocto_cve_qa_info);  

  $('.recipe_info', recipeItem).hide();    
  $('.recipe_details', recipeItem).hide();    
  $('.recipe_cves', recipeItem).hide();    

  if ((cpe_info === "") && (yocto_cve_qa_info === "")) {
    $('.recipe_details', recipeItem).data("empty", "empty");
  }

  if ((recipe.stats.unpatched + recipe.stats.whitelisted + 
       ((include_not_vulnerable) ? recipe.stats.unpatched_not_vulnerable : 0) + recipe.stats.patched) <= 0) {
    $('.recipe_cves', recipeItem).data("empty", "empty");
    $('.recipe_actions', recipeItem).hide();
  }
  
  if ((recipe.stats.unpatched + recipe.stats.whitelisted + 
       ((include_not_vulnerable) ? recipe.stats.unpatched_not_vulnerable : 0) + recipe.stats.patched) > 5) {
    $('.recipe_cves_sort', recipeItem).show();
  }
  else {
    $('.recipe_cves_sort', recipeItem).hide();
  }
  
  if ((recipe.stats.unpatched + recipe.stats.whitelisted + recipe.stats.unpatched_not_vulnerable +
       recipe.stats.patched) > 0) {
   
    var current_cpe_vendor_product  = "";
    var current_cpe_version         = "";
    var current_cve_status          = "";
    var current_cve_list            = $('#CVE_group_unpatched .cve_cves ul')  
    var current_cve_group           = 'unpatched';

    recipe.cpes.forEach(function(cpe){
      var cpe_id = cpe.cpe.split(":");

      current_cpe_vendor_product  = ((cpe_id[0] === "%") ? "" : cpe_id[0]+":") + cpe_id[1];
      current_cpe_version         = cpe_id[2];

      var cveGroupItem = $('<li class="cve_group_li">' + $('#listCVEGroupItem').html() + '</li>');

      current_cve_status = "Unpatched";
      cpe.unpatched.forEach(addCVE);

      current_cve_status = "Whitelisted";
      current_cve_list   = $('#CVE_group_whitelisted .cve_cves ul')  
      current_cve_group  = 'whitelisted';
      cpe.whitelisted.forEach(addCVE);

      current_cve_status = "Patched";
      current_cve_list   = $('#CVE_group_patched .cve_cves ul')  
      current_cve_group  = 'patched';
      cpe.patched.forEach(addCVE);

      if (window.include_not_vulnerable) {
        current_cve_status = "Not vulnerable";
        current_cve_list   = $('#CVE_group_not_vulnerable .cve_cves ul')  
        current_cve_group  = 'not_vulnerable';
        cpe.unpatched_not_vulnerable.forEach(addCVE);
      }
    })
     
    function addCVE(cve) {
      var itemToAppend = $('<li>' + $('#listCVEItem').html() + '</li>');

      $('.cve_status', itemToAppend).html(current_cve_status);
      $('.cve_cpe_vendor_product', itemToAppend).html(current_cpe_vendor_product);
      $('.cve_vector', itemToAppend).html(cve.vector); 
      $('.cve_cpe_version', itemToAppend).html(current_cpe_version);
      $('.cve_id', itemToAppend).html(cve.ID);
      $('.cve_V2_score', itemToAppend).html("V2: "+cve.scorev2);
      $('.cve_V3_score', itemToAppend).html("V3: "+cve.scorev3);
      $('.cve_modified', itemToAppend).html(cve.modified);
      $('.cve_info', itemToAppend).text(cve.summary);
      $('.cve_link', itemToAppend).html('<a href="'+cve.nvd_link+'" target="_blank">'+cve.nvd_link+'</a>');
      if (cve.comment != "") {
        $('.cve_comments', itemToAppend).text(cve.comment);
      }
      else {
        $('.cve_comments', itemToAppend).hide();
        $('.cve_link', itemToAppend).addClass("last_cve_line");
      }
      
      cvss_3_severity       = ((cve.scorev3 == 0)?"none":(cve.scorev3 < 4)?"low":(cve.scorev3 < 7)?"medium":(cve.scorev3 < 9)?"high":"critical");

      cvss_2_severity_class = "cvss_V2_severity_" + ((cve.scorev2 < 4)?"low":(cve.scorev2 < 7)?"medium":"high");
      cvss_3_severity_class = "cvss_V3_severity_" + cvss_3_severity;
      $('.cve_V2_score', itemToAppend).addClass(cvss_2_severity_class);
      $('.cve_V3_score', itemToAppend).addClass(cvss_3_severity_class);       

      if (checkDoubleCVEs(current_cve_group, cve.ID, current_cpe_vendor_product, current_cpe_version)) {
        window.score_totals[current_cve_group]["counts"][cvss_3_severity] += 1;
  
        current_cve_list.append(itemToAppend);
      }

      $('ul', recipeItem).append(itemToAppend.clone());   
    }
  }
  
  $('#recipeList').append(recipeItem);   
}


