/* 
# your_cves_sort.js
#
# This Javascript file is used to generate a web-server 
# or standalone HTML file for Yocto CVE QA and CVE 
# overviews, with your CVEs JSON files as input 
# (generated in Yocto with your_cves_checks).
#
# Copyright (C) 2020 Mads Doré ApS, Denmark.
#
# This source is released under the MIT License.
#
# DISCLAIMER
#
# This class/tool is meant to be used as support and not
# the only method to check against CVEs. Running this tool
# doesn't guarantee your packages are free of CVEs. */

var sorting_in_progress = false;

function your_cves_cve_sort_pressed(who_called, sort_element, sort_type) {
 
  var order = who_called.getAttribute('data-sort');
  var sort_parent = $($(who_called).parent().parent());

  if (window.sorting_in_progress) return;

  window.sorting_in_progress = true;
  
  var test = $("li", sort_parent);

  console.log(test);

  $("li", sort_parent).sort(sort_calc_local).appendTo($("ul", sort_parent));

  function sort_calc_local(a, b) {
    return sort_calc(a, b, order, sort_element, sort_type);
  }

  who_called.setAttribute('data-sort', (who_called.getAttribute('data-sort') == "asc") ? "des" : "asc");

  window.sorting_in_progress = false;
}

function showCVEs(cve_group) {

  var show_it = ($(cve_group).html() === "show");
  var cve_parent = $($(cve_group).parent().parent().parent());

  $(cve_group).html((show_it) ? "hide" : "show");  

  (show_it) ? $('.cve_cves', cve_parent).show() : $('.cve_cves', cve_parent).hide();
}

function your_cves_recipe_cves_sort_pressed(who_called, sort_element, sort_type) {
 
  var order = who_called.getAttribute('data-sort');
  var sort_parent = $($(who_called).parent().parent());

  if (window.sorting_in_progress) return;

  window.sorting_in_progress = true;

  $("li", sort_parent).sort(sort_calc_local).appendTo($("ul", sort_parent));

  function sort_calc_local(a, b) {
    return sort_calc(a, b, order, sort_element, sort_type);
  }

  who_called.setAttribute('data-sort', (who_called.getAttribute('data-sort') == "asc") ? "des" : "asc");

  window.sorting_in_progress = false;
}

function your_cves_sort_recipe_pressed(who_called, sort_element, sort_type) {

  your_cves_sort_recipe(sort_element, who_called.getAttribute('data-sort'), sort_type);
  
  who_called.setAttribute('data-sort', (who_called.getAttribute('data-sort') == "asc") ? "des" : "asc");
}

function your_cves_sort_recipe(sort_element = "div.recipe", order="asc", sort_type = "text") {

  if (window.sorting_in_progress) return;
  window.sorting_in_progress = true;

  $("#recipeList .recipe_li").sort(sort_calc_local).appendTo("#recipeList");

  function sort_calc_local(a, b) {
    return sort_calc(a, b, order, sort_element, sort_type);
  }

  window.sorting_in_progress = false;
}

function sort_calc(a, b, order, sort_element, sort_type) {
    if (order == "asc") {
        if (sort_type == "number") {
          return (Number($(sort_element, a).text()) > Number($(sort_element, b).text())) ? 1 : -1;
        }
        else if (sort_type == "cve_id") {
          var a_content = $(sort_element, a).text();
          var b_content = $(sort_element, b).text();
          var a_split = a_content.split("-");
          var b_split = b_content.split("-");
          var a_year = parseInt(a_split[1]);
          var b_year = parseInt(b_split[1]);

          // a[0] == b[0] == "CVE" is assumbed as cve_id otherwise is malformed.
          if (a_year > b_year) {
            return 1;          
          }
          else if (a_year < b_year) {
            return -1;          
          }

          var a_index = parseInt(a_split[2]);
          var b_index = parseInt(b_split[2]);

          return (a_index > b_index) ? 1 : -1;
        }
        else {
          return (($(sort_element, a).text()) > ($(sort_element, b).text())) ? 1 : -1;
        }
    }
    else if (order == "des") {
        if (sort_type == "number") {
          return (Number($(sort_element, a).text()) < Number($(sort_element, b).text())) ? 1 : -1
        }
        else if (sort_type == "cve_id") {
          var a_content = $(sort_element, a).text();
          var b_content = $(sort_element, b).text();
          var a_split = a_content.split("-");
          var b_split = b_content.split("-");
          var a_year = parseInt(a_split[1]);
          var b_year = parseInt(b_split[1]);

          // a[0] == b[0] == "CVE" is assumbed as cve_id otherwise is malformed.
          if (a_year < b_year) {
            return 1;          
          }
          else if (a_year > b_year) {
            return -1;          
          }

          var a_index = parseInt(a_split[2]);
          var b_index = parseInt(b_split[2]);

          return (a_index < b_index) ? 1 : -1;
        }
        else {
          return (($(sort_element, a).text()) < ($(sort_element, b).text())) ? 1 : -1;
        }
    }
    return 0;    
}

function showAllDetailsRecipe(recipe) {

  var all = ($(recipe).html() === "show");

  $(recipe).html((all) ? "hide" : "show");  

  showDetailsRecipe(recipe, all, all, all);
}

function showDetailsRecipe(recipe, info, details, cves) {
  var li_element = $(recipe).closest("li");

  $(recipe).html((info) ? "hide" : "show");  

  (info) ? $('.recipe_info', li_element).show() : $('.recipe_info', li_element).hide();
  if ($('.recipe_details', li_element).data("empty") != "empty") {
    (details) ? $('.recipe_details', li_element).show() : $('.recipe_details', li_element).hide();
  }

  if ($('.recipe_cves', li_element).data("empty") != "empty") {
    (cves) ? $('.recipe_cves', li_element).show() : $('.recipe_cves', li_element).hide();
  }
}


